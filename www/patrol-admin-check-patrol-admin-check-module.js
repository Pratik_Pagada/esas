(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["patrol-admin-check-patrol-admin-check-module"],{

/***/ "./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ng-container *ngIf=\"selectedCriteria == null\">\r\n    <ion-grid>\r\n        <ion-row>\r\n            <ion-col>\r\n                <app-layout [multi]=\"false\" [default]=\"default\" (placeSelected)='refreshPlace($event)'></app-layout>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <ion-input type=\"text\" placeholder=\"Pontos hely\" [(ngModel)]=\"checkpoint.exactPlace\"></ion-input>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <ion-input type=\"text\" placeholder=\"Ellenőrzőpont neve\" [(ngModel)]=\"checkpoint.name\"></ion-input>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row >\r\n            <ion-col>\r\n                <ion-button color=\"primary\" (click)=\"takePicture()\">\r\n                        <ion-icon slot=\"start\" name=\"camera\"></ion-icon>Kép feltöltése\r\n                </ion-button> \r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"checkpoint.photo != null\">\r\n            <ion-col size=\"12\">\r\n                <img style=\"width:100%;\" [src]=\"domSanitizer.bypassSecurityTrustUrl(checkpoint.photo)\" />\r\n                <ion-button color=\"danger\" expand=\"block\" (click)=\"checkpoint.photo = null;\">\r\n                    <ion-icon name=\"close\"></ion-icon>\r\n                </ion-button> \r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                Ellenőrzési kritériumok\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngFor =\"let criteria of checkpoint.checkAttributes\" align-items-center>\r\n            <ion-col size=\"8\">\r\n                {{criteria.name}}\r\n            </ion-col>\r\n            <ion-col size=\"2\">\r\n                <ion-button color=\"primary\" (click)=\"editCriteria(criteria)\">\r\n                    <ion-icon slot=\"icon-only\" name=\"create\"></ion-icon>\r\n                </ion-button>\r\n            </ion-col>\r\n            <ion-col size=\"2\">\r\n                <ion-button color=\"danger\" (click)=\"deleteCriteria(criteria)\">\r\n                    <ion-icon slot=\"icon-only\" name=\"trash\"></ion-icon>\r\n                </ion-button>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n    <ion-button color=\"primary\" expand=\"full\" (click)=\"newCriteria()\" *ngIf=\"checkpoint.id\">Új kritérium</ion-button>\r\n    <ion-button color=\"success\" expand=\"full\" (click)=\"save()\">Mentés</ion-button>\r\n</ng-container>\r\n\r\n<ng-container *ngIf=\"selectedCriteria != null\">\r\n    <app-patrol-admin-criteria-form [criteria]=\"selectedCriteria\" (finishedEdit)=\"onFinished($event)\" ></app-patrol-admin-criteria-form>\r\n</ng-container>\r\n"

/***/ }),

/***/ "./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdHJvbC9hZG1pbi9jaGVjay9jaGVjay1mb3JtL3BhdHJvbC1hZG1pbi1jaGVjay1mb3JtLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.ts ***!
  \************************************************************************************/
/*! exports provided: PatrolAdminCheckFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolAdminCheckFormComponent", function() { return PatrolAdminCheckFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_patrol_patrol_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/patrol/patrol.service */ "./src/app/patrol/patrol.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _ionic_native_action_sheet_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/action-sheet/ngx */ "./node_modules/@ionic-native/action-sheet/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PatrolAdminCheckFormComponent = /** @class */ (function () {
    function PatrolAdminCheckFormComponent(_patrolService, camera, domSanitizer, actionSheet, alertCtrl) {
        this._patrolService = _patrolService;
        this.camera = camera;
        this.domSanitizer = domSanitizer;
        this.actionSheet = actionSheet;
        this.alertCtrl = alertCtrl;
        this.finishedEdit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.selectedCriteria = null;
        this.newCrit = false;
        this.editCrit = false;
        this.default = null;
    }
    PatrolAdminCheckFormComponent.prototype.ngOnInit = function () {
        this.default = this.checkpoint.place.id;
    };
    PatrolAdminCheckFormComponent.prototype.refreshPlace = function (place) {
        this.checkpoint.placeId = place;
    };
    PatrolAdminCheckFormComponent.prototype.takePicture = function () {
        var _this = this;
        var buttonLabels = ['Fénykép készítése', 'Galéria'];
        var options = {
            title: 'Kép forrása',
            buttonLabels: buttonLabels,
            addCancelButtonWithLabel: 'Cancel',
            destructiveButtonLast: true
        };
        this.actionSheet.show(options).then(function (buttonIndex) {
            if (buttonIndex == 1) {
                _this.doTakePicture(_this.camera.PictureSourceType.CAMERA);
            }
            else {
                _this.doTakePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
            }
        });
    };
    PatrolAdminCheckFormComponent.prototype.doTakePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 30,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.checkpoint.photo = base64Image;
            _this.file = _this.dataURLtoBlob(base64Image);
        });
    };
    PatrolAdminCheckFormComponent.prototype.dataURLtoBlob = function (dataurl) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1], bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
    };
    PatrolAdminCheckFormComponent.prototype.editCriteria = function (criteria) {
        this.selectedCriteria = criteria;
    };
    PatrolAdminCheckFormComponent.prototype.deleteCriteria = function (criteria) {
        var index = this.checkpoint.criteria.indexOf(criteria);
        if (index > -1) {
            this.checkpoint.criteria.splice(index, 1);
        }
    };
    PatrolAdminCheckFormComponent.prototype.newCriteria = function () {
        this.selectedCriteria = {
            name: '',
            description: '',
            photos: [],
            actions: [],
            actionIdList: [],
            photoMandatory: false
        };
        this.newCrit = true;
        this.editCrit = true;
    };
    PatrolAdminCheckFormComponent.prototype.onFinished = function (event) {
        if (this.newCrit) {
            this.selectedCriteria.checkId = this.checkpoint.id;
            this.checkpoint.checkAttributes.push(this.selectedCriteria);
            this._patrolService.saveAttribute(this.selectedCriteria);
            this._patrolService.refreshChecks();
        }
        this.newCrit = false;
        this.selectedCriteria = null;
    };
    PatrolAdminCheckFormComponent.prototype.save = function () {
        var _this = this;
        if (this.new) {
            this._patrolService.addCheck(this.checkpoint).subscribe(function (data) {
                if (_this.file) {
                    _this._patrolService.uploadFile(data.id, 'PATROL_CHECK_PHOTO', _this.file).subscribe(function (data) {
                    });
                }
                _this._patrolService.refreshChecks();
            });
        }
        this.finishedEdit.emit(true);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatrolAdminCheckFormComponent.prototype, "checkpoint", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatrolAdminCheckFormComponent.prototype, "new", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PatrolAdminCheckFormComponent.prototype, "finishedEdit", void 0);
    PatrolAdminCheckFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patrol-admin-check-form',
            template: __webpack_require__(/*! ./patrol-admin-check-form.component.html */ "./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.html"),
            styles: [__webpack_require__(/*! ./patrol-admin-check-form.component.scss */ "./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_patrol_patrol_service__WEBPACK_IMPORTED_MODULE_3__["PatrolService"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["DomSanitizer"], _ionic_native_action_sheet_ngx__WEBPACK_IMPORTED_MODULE_5__["ActionSheet"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], PatrolAdminCheckFormComponent);
    return PatrolAdminCheckFormComponent;
}());



/***/ }),

/***/ "./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.html":
/*!********************************************************************************************!*\
  !*** ./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.html ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-grid>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-input type=\"text\" placeholder=\"Kritérium neve\" [(ngModel)]=\"criteria.name\"></ion-input>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-input type=\"text\" placeholder=\"Kritérium leírása\" [(ngModel)]=\"criteria.description\"></ion-input>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-button color=\"primary\" (click)=\"takePicture()\">\r\n                <ion-icon slot=\"start\" name=\"camera\"></ion-icon>Kép feltöltése\r\n            </ion-button> \r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col size=\"4\" *ngFor=\"let image of criteria.photos; let i = index\">\r\n            <img style=\"width:100%;\"  [src]=\"domSanitizer.bypassSecurityTrustUrl(image)\" />\r\n            <ion-button color=\"danger\" expand=\"block\" (click)=\"removeImage(i)\">\r\n                    <ion-icon name=\"close\"></ion-icon>\r\n            </ion-button> \r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-item>\r\n                <ion-label>Kép készítése kötelező</ion-label>\r\n                <ion-checkbox color=\"dark\" [(ngModel)]=\"criteria.photoMandatory\"></ion-checkbox>\r\n            </ion-item>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row *ngFor=\"let formAction of criteria.actionIdList; let i = index\">\r\n        <ion-select [(ngModel)]=\"criteria.actionIdList[i]\">\r\n            <ion-select-option *ngFor=\"let action of actions\" value=\"{{action.id}}\">{{ action.name }}</ion-select-option>\r\n        </ion-select>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-button color=\"success\" (click)=\"addAction()\">Intézkedés hozzáadása</ion-button>\r\n    </ion-row>\r\n</ion-grid>\r\n<ion-button color=\"success\" expand=\"full\" (click)=\"save()\" [disabled]=\"criteria.actionIdList.length == 0\">Mentés</ion-button>\r\n"

/***/ }),

/***/ "./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.scss":
/*!********************************************************************************************!*\
  !*** ./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.scss ***!
  \********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdHJvbC9hZG1pbi9jaGVjay9jcml0ZXJpYS1mb3JtL3BhdHJvbC1hZG1pbi1jcml0ZXJpYS1mb3JtLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.ts":
/*!******************************************************************************************!*\
  !*** ./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.ts ***!
  \******************************************************************************************/
/*! exports provided: PatrolAdminCriteriaFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolAdminCriteriaFormComponent", function() { return PatrolAdminCriteriaFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_patrol_patrol_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/patrol/patrol.service */ "./src/app/patrol/patrol.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_action_sheet_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/action-sheet/ngx */ "./node_modules/@ionic-native/action-sheet/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var PatrolAdminCriteriaFormComponent = /** @class */ (function () {
    function PatrolAdminCriteriaFormComponent(_patrolService, camera, domSanitizer, file, toastCtrl, actionSheet) {
        this._patrolService = _patrolService;
        this.camera = camera;
        this.domSanitizer = domSanitizer;
        this.file = file;
        this.toastCtrl = toastCtrl;
        this.actionSheet = actionSheet;
        this.finishedEdit = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.files = [];
    }
    PatrolAdminCriteriaFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._patrolService.getAllActions().subscribe(function (data) {
            _this.actions = data.content;
        });
        if (this.criteria.id) {
            if (!this.criteria.actionIdList) {
                this.criteria.actionIdList = [];
            }
            this.criteria.actions.forEach(function (data) {
                _this.criteria.actionIdList.push(data.id);
            });
        }
    };
    PatrolAdminCriteriaFormComponent.prototype.takePicture = function () {
        var _this = this;
        var buttonLabels = ['Fénykép készítése', 'Galéria'];
        var options = {
            title: 'Kép forrása',
            buttonLabels: buttonLabels,
            addCancelButtonWithLabel: 'Cancel',
            destructiveButtonLast: true
        };
        this.actionSheet.show(options).then(function (buttonIndex) {
            if (buttonIndex == 1) {
                _this.doTakePicture(_this.camera.PictureSourceType.CAMERA);
            }
            else {
                _this.doTakePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
            }
        });
    };
    PatrolAdminCriteriaFormComponent.prototype.doTakePicture = function (sourceType) {
        var _this = this;
        var options = {
            quality: 30,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            if (!_this.criteria.photos) {
                _this.criteria.photos = [];
            }
            if (!_this.criteria.files) {
                _this.criteria.files = [];
            }
            _this.criteria.photos.push(base64Image);
            _this.criteria.files.push(base64Image);
        });
    };
    PatrolAdminCriteriaFormComponent.prototype.dataURLtoBlob = function (dataurl) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1], bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], { type: mime });
    };
    PatrolAdminCriteriaFormComponent.prototype.addAction = function () {
        if (!this.criteria.actionIdList) {
            this.criteria.actionIdList = [];
        }
        this.criteria.actionIdList.push(this.actions[0].id);
    };
    PatrolAdminCriteriaFormComponent.prototype.removeImage = function (index) {
        this.criteria.photos.splice(index, 1);
    };
    PatrolAdminCriteriaFormComponent.prototype.save = function () {
        this.finishedEdit.emit(this.criteria);
    };
    PatrolAdminCriteriaFormComponent.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName) {
        var _this = this;
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function (success) {
            _this.criteria.photos.push(newFileName);
        }, function (error) {
        });
    };
    PatrolAdminCriteriaFormComponent.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Always get the accurate path to your apps folder
    PatrolAdminCriteriaFormComponent.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatrolAdminCriteriaFormComponent.prototype, "criteria", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PatrolAdminCriteriaFormComponent.prototype, "finishedEdit", void 0);
    PatrolAdminCriteriaFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patrol-admin-criteria-form',
            template: __webpack_require__(/*! ./patrol-admin-criteria-form.component.html */ "./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.html"),
            styles: [__webpack_require__(/*! ./patrol-admin-criteria-form.component.scss */ "./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.scss")]
        }),
        __metadata("design:paramtypes", [src_app_patrol_patrol_service__WEBPACK_IMPORTED_MODULE_3__["PatrolService"], _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["DomSanitizer"], _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__["File"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"], _ionic_native_action_sheet_ngx__WEBPACK_IMPORTED_MODULE_6__["ActionSheet"]])
    ], PatrolAdminCriteriaFormComponent);
    return PatrolAdminCriteriaFormComponent;
}());



/***/ }),

/***/ "./src/app/patrol/admin/check/patrol-admin-check.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/patrol/admin/check/patrol-admin-check.module.ts ***!
  \*****************************************************************/
/*! exports provided: PatrolAdminCheckPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolAdminCheckPageModule", function() { return PatrolAdminCheckPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_layout_layout_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/layout/layout.module */ "./src/app/layout/layout.module.ts");
/* harmony import */ var src_app_header_header_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/header/header.module */ "./src/app/header/header.module.ts");
/* harmony import */ var _patrol_admin_check_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./patrol-admin-check.page */ "./src/app/patrol/admin/check/patrol-admin-check.page.ts");
/* harmony import */ var _patrol_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../patrol.service */ "./src/app/patrol/patrol.service.ts");
/* harmony import */ var _check_form_patrol_admin_check_form_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./check-form/patrol-admin-check-form.component */ "./src/app/patrol/admin/check/check-form/patrol-admin-check-form.component.ts");
/* harmony import */ var _criteria_form_patrol_admin_criteria_form_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./criteria-form/patrol-admin-criteria-form.component */ "./src/app/patrol/admin/check/criteria-form/patrol-admin-criteria-form.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var PatrolAdminCheckPageModule = /** @class */ (function () {
    function PatrolAdminCheckPageModule() {
    }
    PatrolAdminCheckPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                src_app_header_header_module__WEBPACK_IMPORTED_MODULE_6__["HeaderModule"],
                src_app_layout_layout_module__WEBPACK_IMPORTED_MODULE_5__["LayoutComponentModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _patrol_admin_check_page__WEBPACK_IMPORTED_MODULE_7__["PatrolAdminCheckPage"]
                    }
                ])
            ],
            declarations: [_patrol_admin_check_page__WEBPACK_IMPORTED_MODULE_7__["PatrolAdminCheckPage"], _check_form_patrol_admin_check_form_component__WEBPACK_IMPORTED_MODULE_9__["PatrolAdminCheckFormComponent"], _criteria_form_patrol_admin_criteria_form_component__WEBPACK_IMPORTED_MODULE_10__["PatrolAdminCriteriaFormComponent"]],
            providers: [_patrol_service__WEBPACK_IMPORTED_MODULE_8__["PatrolService"]]
        })
    ], PatrolAdminCheckPageModule);
    return PatrolAdminCheckPageModule;
}());



/***/ }),

/***/ "./src/app/patrol/admin/check/patrol-admin-check.page.html":
/*!*****************************************************************!*\
  !*** ./src/app/patrol/admin/check/patrol-admin-check.page.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [headerText]=\"'Ellenőrzőpont szerkesztése'\"></app-header>\r\n\r\n<ng-container *ngIf=\"!edit && !new\">\r\n    <ion-content>\r\n        <ion-list lines=\"none\">\r\n            <ion-list-header>\r\n                <ion-label>Ellenőrzőpontok</ion-label>\r\n            </ion-list-header>\r\n            <ion-item *ngFor=\"let check of checks\">\r\n                <ion-label slot=\"start\">{{check.name}}</ion-label>\r\n                <ion-label>{{check.place.hungarianName}}</ion-label>\r\n                <ion-button (click)=\"editCheck(check)\">\r\n                    <ion-icon slot=\"icon-only\" name=\"create\"></ion-icon>\r\n                </ion-button>\r\n                <ion-button color=\"danger\" (click)=\"deleteCheck(check)\">\r\n                    <ion-icon slot=\"icon-only\" name=\"trash\"></ion-icon>\r\n                </ion-button>\r\n            </ion-item>\r\n        </ion-list>\r\n        <ion-button color=\"primary\" expand=\"full\" (click)=\"createCheck()\">Új pont felvétele</ion-button>\r\n    </ion-content>\r\n\r\n    \r\n</ng-container>\r\n\r\n<ion-content *ngIf = \"new || edit\">\r\n    <app-patrol-admin-check-form [checkpoint]=\"selectedCheckpoint\" [new]=\"new\" (finishedEdit)=\"onFinished($event)\"></app-patrol-admin-check-form>\r\n</ion-content>"

/***/ }),

/***/ "./src/app/patrol/admin/check/patrol-admin-check.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/patrol/admin/check/patrol-admin-check.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdHJvbC9hZG1pbi9jaGVjay9wYXRyb2wtYWRtaW4tY2hlY2sucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/patrol/admin/check/patrol-admin-check.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/patrol/admin/check/patrol-admin-check.page.ts ***!
  \***************************************************************/
/*! exports provided: PatrolAdminCheckPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolAdminCheckPage", function() { return PatrolAdminCheckPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _patrol_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../patrol.service */ "./src/app/patrol/patrol.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PatrolAdminCheckPage = /** @class */ (function () {
    function PatrolAdminCheckPage(_patrolService) {
        this._patrolService = _patrolService;
        this.checks = [];
        this.selectedCheckpoint = null;
        this.edit = false;
        this.new = false;
    }
    PatrolAdminCheckPage.prototype.ngOnInit = function () {
        var _this = this;
        this._patrolService.checkSubscription.subscribe(function (data) {
            _this.checks = [];
            _this.checks = data;
            console.log("updated");
            console.log(_this.checks);
        });
        this.refreshChecks();
    };
    PatrolAdminCheckPage.prototype.refreshChecks = function () {
        this._patrolService.getChecks();
    };
    PatrolAdminCheckPage.prototype.deleteCheck = function (check) {
        this._patrolService.removeCheck(check);
        this.refreshChecks();
    };
    PatrolAdminCheckPage.prototype.createCheck = function () {
        this.selectedCheckpoint = {
            name: '',
            place: '',
            image: null,
            criteria: []
        };
        this.new = true;
    };
    PatrolAdminCheckPage.prototype.editCheck = function (check) {
        this.selectedCheckpoint = check;
        this.edit = true;
    };
    PatrolAdminCheckPage.prototype.onFinished = function (event) {
        this.new = false;
        this.edit = false;
        this.selectedCheckpoint = null;
        this.refreshChecks();
    };
    PatrolAdminCheckPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patrol-admin-check',
            template: __webpack_require__(/*! ./patrol-admin-check.page.html */ "./src/app/patrol/admin/check/patrol-admin-check.page.html"),
            changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].Default,
            styles: [__webpack_require__(/*! ./patrol-admin-check.page.scss */ "./src/app/patrol/admin/check/patrol-admin-check.page.scss")]
        }),
        __metadata("design:paramtypes", [_patrol_service__WEBPACK_IMPORTED_MODULE_1__["PatrolService"]])
    ], PatrolAdminCheckPage);
    return PatrolAdminCheckPage;
}());



/***/ })

}]);
//# sourceMappingURL=patrol-admin-check-patrol-admin-check-module.js.map