(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["patrol-guard-patrol-guard-module"],{

/***/ "./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-grid *ngIf=\"!started\">\r\n    <ion-row>\r\n        <ion-col>\r\n        <div text-center>\r\n            <h1>{{progress}}. ellenőrzőpont</h1>\r\n        </div>\r\n        </ion-col>\r\n    </ion-row> \r\n    <ion-row>\r\n        <ion-col>\r\n        <div text-center>\r\n            <h2>Helye: {{checkpoint.check.check.place.hungarianName}}</h2>\r\n        </div>\r\n        \r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n        <div text-center>\r\n            <h2>Neve: {{checkpoint.check.check.name}}</h2>\r\n        </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <div text-center>\r\n               <img [src]=\"apiUrl+checkpoint.check.check.photoPath\"/>\r\n            </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-spinner *ngIf=\"loading\"></ion-spinner>\r\n            <ion-button *ngIf=\"!loading\" color=\"primary\" expand=\"full\" (click)=\"startCheckpoint()\">Ellenőrzés megkezdése</ion-button>\r\n            <ion-button *ngIf=\"!loading\" color=\"warning\" expand=\"full\">Kihagyás</ion-button>\r\n        </ion-col>\r\n    </ion-row>\r\n</ion-grid>\r\n\r\n<ion-grid *ngIf=\"started && choice == null\">\r\n    <ion-row>\r\n        <ion-col>\r\n            <div text-center>\r\n               {{checkpoint.check.check.name}}\r\n            </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <div text-center>\r\n                <h1>{{currentCriteria.checkAttribute.checkAttribute.name}}</h1>\r\n            </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <div text-center>\r\n                <img  *ngFor=\"let photo of currentCriteria.checkAttribute.checkAttribute.photos\" [src]=\"apiUrl+photo.path\"/>\r\n            </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <div text-center>\r\n                {{currentCriteria.checkAttribute.checkAttribute.description}}\r\n            </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-button color=\"success\" expand=\"full\" (click)=\"ok()\">IGEN</ion-button>\r\n            <ion-button color=\"danger\" expand=\"full\" (click)=\"nok()\">NEM</ion-button>\r\n            <ion-button color=\"warning\" expand=\"full\" (click)=\"skip()\">Kihagyás</ion-button>\r\n        </ion-col>\r\n    </ion-row>\r\n</ion-grid>\r\n\r\n<ng-container *ngIf=\"choice != null\">\r\n    <ion-grid>\r\n        <ion-row *ngIf=\"choice == -1\">\r\n            <ion-col>\r\n                Intézkedések\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"choice == -1\">\r\n            <ion-col>\r\n                <ion-item *ngFor=\"let action of actions\">\r\n                    <ion-label>{{ action.name }}</ion-label>\r\n                    <ion-checkbox color=\"dark\" [(ngModel)]=\"action.checked\"></ion-checkbox>\r\n                </ion-item>\r\n            </ion-col>\r\n        </ion-row>\r\n            \r\n        <ion-row *ngIf=\"choice != 1\"> \r\n            <ion-col>\r\n                <ion-textarea placeholder=\"Leírás\" [(ngModel)]=\"description\"></ion-textarea>\r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"choice != 0\">\r\n            <ion-col>\r\n                <ion-button color=\"primary\" (click)=\"takePicture()\">\r\n                        <ion-icon slot=\"start\" name=\"camera\"></ion-icon>Kép feltöltése\r\n                </ion-button> \r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row *ngIf=\"choice != 0\">\r\n            <ion-col size=\"4\" *ngFor=\"let image of images; let i = index\">\r\n                <img style=\"width:100%;\"  [src]=\"domSanitizer.bypassSecurityTrustUrl(image)\" />\r\n                <ion-button color=\"danger\" expand=\"block\" (click)=\"removeImage(i)\">\r\n                        <ion-icon name=\"close\"></ion-icon>\r\n                </ion-button> \r\n            </ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n            <ion-col>\r\n                <ion-spinner *ngIf=\"loading\"></ion-spinner>\r\n                <ion-button *ngIf=\"!loading\" color=\"primary\" expand=\"full\" (click)=\"sendChoice()\" [disabled]=\"!sendResultValid()\">Elküld</ion-button>\r\n            </ion-col>\r\n        </ion-row>\r\n    </ion-grid>\r\n</ng-container>\r\n"

/***/ }),

/***/ "./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  height: 60px;\n  font-size: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9teW1hYy9Eb2N1bWVudHMvR2l0UHJvamVjdHMvRS1zYXMtSU9OSUMvc3JjL2FwcC9wYXRyb2wvZ3VhcmQvY2hlY2twb2ludC9wYXRyb2wtY2hlY2twb2ludC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQVc7RUFDWCxlQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYXRyb2wvZ3VhcmQvY2hlY2twb2ludC9wYXRyb2wtY2hlY2twb2ludC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xyXG4gICAgaGVpZ2h0OjYwcHg7XHJcbiAgICBmb250LXNpemU6MjBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.ts ***!
  \************************************************************************/
/*! exports provided: PatrolCheckpointComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolCheckpointComponent", function() { return PatrolCheckpointComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _patrol_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../patrol.service */ "./src/app/patrol/patrol.service.ts");
/* harmony import */ var src_app_app_settings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/app.settings */ "./src/app/app.settings.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PatrolCheckpointComponent = /** @class */ (function () {
    function PatrolCheckpointComponent(camera, domSanitizer, _patrolService) {
        this.camera = camera;
        this.domSanitizer = domSanitizer;
        this._patrolService = _patrolService;
        this.finishedCheckpoint = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.startedCheckpoint = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.images = [];
        this.choice = null;
        this.description = '';
        this.started = false;
        this.loading = false;
        this.criteriaProgress = 1;
    }
    PatrolCheckpointComponent.prototype.ngOnInit = function () {
        console.log(this.checkpoint);
    };
    PatrolCheckpointComponent.prototype.startCheckpoint = function () {
        var _this = this;
        this.loading = true;
        this._patrolService.startCheckInstance(this.checkpoint.id).subscribe(function (data) {
            _this.startedCheckpoint.emit(true);
            _this.started = true;
            _this.criteriaProgress = 1;
            _this.loading = false;
        });
    };
    PatrolCheckpointComponent.prototype.ok = function () {
        this.choice = 1;
    };
    PatrolCheckpointComponent.prototype.nok = function () {
        this.choice = -1;
    };
    PatrolCheckpointComponent.prototype.skip = function () {
        this.choice = 0;
    };
    PatrolCheckpointComponent.prototype.sendResultValid = function () {
        var valid = true;
        this.actions.forEach(function (element) {
            if (!element.checked) {
                valid = false;
            }
        });
        return this.choice != -1 || true || (valid && (this.images.length > 0));
    };
    PatrolCheckpointComponent.prototype.sendChoice = function () {
        var _this = this;
        var data = null;
        if (this.choice == 0) {
            /*
            var data = {
                instanceId: this.checkpoint.id,
                skipReason: this.description
            }
            this._patrolService.skipCheckInstance();
            */
        }
        this.loading = true;
        if (this.choice == -1) {
            data = {
                checkAttributeInstancekId: this.currentCriteria.id,
                description: this.description,
                photos: this.images,
                severity: "LOW",
                responsibleUserId: "jJKXJyKzwm"
            };
            this._patrolService.saveIrregularity(data).subscribe(function (d) {
                data = {
                    checkAttributeInstanceId: _this.currentCriteria.id,
                    result: "IRREGULAR",
                    photos: _this.images
                };
                _this._patrolService.saveCheckAttributeInstance(data).subscribe(function (z) {
                    console.log("step");
                    console.log(_this.checkpoint.checkAttributeInstances.length);
                    if (_this.checkpoint.checkAttributeInstances[_this.criteriaProgress]) {
                        _this.criteriaProgress++;
                    }
                    else {
                        _this.started = false;
                        _this.finishedCheckpoint.emit(true);
                    }
                    _this.description = '';
                    _this.images = [];
                    _this.choice = null;
                    _this.loading = false;
                });
            });
        }
        else if (this.choice == 1) {
            data = {
                checkAttributeInstanceId: this.currentCriteria.id,
                result: "OK",
                photos: this.images
            };
            this._patrolService.saveCheckAttributeInstance(data).subscribe(function (d) {
                if (_this.checkpoint.checkAttributeInstances[_this.criteriaProgress]) {
                    _this.criteriaProgress++;
                }
                else {
                    _this.started = false;
                    _this.finishedCheckpoint.emit(true);
                }
                _this.description = '';
                _this.images = [];
                _this.choice = null;
                _this.loading = false;
            });
        }
    };
    Object.defineProperty(PatrolCheckpointComponent.prototype, "currentCriteria", {
        get: function () {
            return this.checkpoint.checkAttributeInstances[this.criteriaProgress - 1];
        },
        enumerable: true,
        configurable: true
    });
    PatrolCheckpointComponent.prototype.takePicture = function () {
        var _this = this;
        var options = {
            quality: 30,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.images.push(base64Image);
        });
    };
    PatrolCheckpointComponent.prototype.removeImage = function (index) {
        this.images.splice(index, 1);
    };
    Object.defineProperty(PatrolCheckpointComponent.prototype, "actions", {
        get: function () {
            return this.currentCriteria.checkAttribute.actions;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PatrolCheckpointComponent.prototype, "apiUrl", {
        get: function () {
            return src_app_app_settings__WEBPACK_IMPORTED_MODULE_4__["AppSettings"].PATROL_BASE_URL + '/';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatrolCheckpointComponent.prototype, "checkpoint", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], PatrolCheckpointComponent.prototype, "progress", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PatrolCheckpointComponent.prototype, "finishedCheckpoint", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], PatrolCheckpointComponent.prototype, "startedCheckpoint", void 0);
    PatrolCheckpointComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patrol-guard-checkpoint',
            template: __webpack_require__(/*! ./patrol-checkpoint.component.html */ "./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.html"),
            styles: [__webpack_require__(/*! ./patrol-checkpoint.component.scss */ "./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"], _patrol_service__WEBPACK_IMPORTED_MODULE_3__["PatrolService"]])
    ], PatrolCheckpointComponent);
    return PatrolCheckpointComponent;
}());



/***/ }),

/***/ "./src/app/patrol/guard/custom-perception/custom-perception.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/patrol/guard/custom-perception/custom-perception.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-grid>\r\n    <ion-row>\r\n        <ion-col>\r\n            <div text-center>\r\n                <h1>Egyéni észrevétel</h1>\r\n            </div>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-textarea placeholder=\"Leírás\" [(ngModel)]=\"description\"></ion-textarea>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-button color=\"primary\" (click)=\"takePicture()\">\r\n                    <ion-icon slot=\"start\" name=\"camera\"></ion-icon>Kép feltöltése\r\n            </ion-button> \r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col size=\"4\" *ngFor=\"let image of images; let i = index\">\r\n            <img style=\"width:100%;\"  [src]=\"domSanitizer.bypassSecurityTrustUrl(image)\" />\r\n            <ion-button color=\"danger\" expand=\"block\" (click)=\"removeImage(i)\">\r\n                <ion-icon name=\"close\"></ion-icon>\r\n            </ion-button> \r\n        </ion-col>\r\n    </ion-row>\r\n</ion-grid>"

/***/ }),

/***/ "./src/app/patrol/guard/custom-perception/custom-perception.component.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/patrol/guard/custom-perception/custom-perception.component.scss ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhdHJvbC9ndWFyZC9jdXN0b20tcGVyY2VwdGlvbi9jdXN0b20tcGVyY2VwdGlvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/patrol/guard/custom-perception/custom-perception.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/patrol/guard/custom-perception/custom-perception.component.ts ***!
  \*******************************************************************************/
/*! exports provided: CustomPerceptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomPerceptionComponent", function() { return CustomPerceptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/Camera/ngx */ "./node_modules/@ionic-native/Camera/ngx/index.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomPerceptionComponent = /** @class */ (function () {
    function CustomPerceptionComponent(camera, domSanitizer) {
        this.camera = camera;
        this.domSanitizer = domSanitizer;
        this.description = '';
        this.images = [];
    }
    CustomPerceptionComponent.prototype.ngOnInit = function () {
    };
    CustomPerceptionComponent.prototype.takePicture = function () {
        var _this = this;
        var options = {
            quality: 30,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.images.push(base64Image);
        });
    };
    CustomPerceptionComponent.prototype.removeImage = function (index) {
        this.images.splice(index, 1);
    };
    CustomPerceptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patrol-custom-perception',
            template: __webpack_require__(/*! ./custom-perception.component.html */ "./src/app/patrol/guard/custom-perception/custom-perception.component.html"),
            styles: [__webpack_require__(/*! ./custom-perception.component.scss */ "./src/app/patrol/guard/custom-perception/custom-perception.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_native_Camera_ngx__WEBPACK_IMPORTED_MODULE_1__["Camera"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"]])
    ], CustomPerceptionComponent);
    return CustomPerceptionComponent;
}());



/***/ }),

/***/ "./src/app/patrol/guard/patrol-guard.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/patrol/guard/patrol-guard.module.ts ***!
  \*****************************************************/
/*! exports provided: PatrolGuardPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolGuardPageModule", function() { return PatrolGuardPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../header/header.module */ "./src/app/header/header.module.ts");
/* harmony import */ var _layout_layout_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../layout/layout.module */ "./src/app/layout/layout.module.ts");
/* harmony import */ var _patrol_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../patrol.service */ "./src/app/patrol/patrol.service.ts");
/* harmony import */ var _patrol_guard_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./patrol-guard.page */ "./src/app/patrol/guard/patrol-guard.page.ts");
/* harmony import */ var _checkpoint_patrol_checkpoint_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./checkpoint/patrol-checkpoint.component */ "./src/app/patrol/guard/checkpoint/patrol-checkpoint.component.ts");
/* harmony import */ var _custom_perception_custom_perception_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./custom-perception/custom-perception.component */ "./src/app/patrol/guard/custom-perception/custom-perception.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var PatrolGuardPageModule = /** @class */ (function () {
    function PatrolGuardPageModule() {
    }
    PatrolGuardPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _header_header_module__WEBPACK_IMPORTED_MODULE_5__["HeaderModule"],
                _layout_layout_module__WEBPACK_IMPORTED_MODULE_6__["LayoutComponentModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _patrol_guard_page__WEBPACK_IMPORTED_MODULE_8__["PatrolGuardPage"]
                    }
                ])
            ],
            declarations: [_patrol_guard_page__WEBPACK_IMPORTED_MODULE_8__["PatrolGuardPage"], _checkpoint_patrol_checkpoint_component__WEBPACK_IMPORTED_MODULE_9__["PatrolCheckpointComponent"], _custom_perception_custom_perception_component__WEBPACK_IMPORTED_MODULE_10__["CustomPerceptionComponent"]],
            providers: [_patrol_service__WEBPACK_IMPORTED_MODULE_7__["PatrolService"]]
        })
    ], PatrolGuardPageModule);
    return PatrolGuardPageModule;
}());



/***/ }),

/***/ "./src/app/patrol/guard/patrol-guard.page.html":
/*!*****************************************************!*\
  !*** ./src/app/patrol/guard/patrol-guard.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [headerText]=\"'Bejárás'\"></app-header>\n<ion-button color=\"primary\" expand=\"full\" *ngIf=\"!customPerception\" (click)=\"customPerception = true; images = [];\">Észrevétel felvétele</ion-button>\n<ion-button color=\"primary\" *ngIf=\"customPerception\" (click)=\"customPerception = false\">Vissza</ion-button>\n\n<ion-content *ngIf=\"!inProgress && !finished && !customPerception && !skip\">\n  <ion-grid>\n    <ion-row>\n      <ion-col>\n        <div text-center>\n            <h1>{{currentPatrol.name}}</h1>\n        </div>\n      </ion-col>\n    </ion-row> \n    <ion-row>\n      <ion-col>\n        <div text-center>\n          <h2>{{currentPatrol.instances[0].date}}</h2>\n        </div>\n        \n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <div text-center>\n            {{currentPatrol.owner}}\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <ion-spinner *ngIf=\"loading\"></ion-spinner>\n          <ion-button *ngIf=\"!loading\" expand=\"full\" (click)=\"startPatrol()\">Bejárás megkezdése</ion-button>\n          <ion-button *ngIf=\"!loading\" color = \"warning\" expand=\"full\" (click)=\"skip = true;\">Bejárás elmarad</ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-content *ngIf=\"inProgress && !customPerception && !skip && !finished\">\n  <app-patrol-guard-checkpoint [checkpoint]=\"currentCheckpoint\" [progress]=\"progress\" (startedCheckpoint)=\"onCheckpointStarted($event)\" (finishedCheckpoint)=\"onFinished($event)\"></app-patrol-guard-checkpoint>\n</ion-content>\n\n<ion-content *ngIf=\"finished && !customPerception && !skip\">\n    <ion-grid>\n      <ion-row>\n          <ion-col>\n            <div text-center>\n                <h1>A bejárás befejeződött.</h1>\n            </div>\n          </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n<ion-content *ngIf=\"skip\">\n    <ion-grid>\n        <ion-row> \n            <ion-col>\n                <ion-textarea placeholder=\"Indoklás\"></ion-textarea>\n            </ion-col>\n        </ion-row>\n        <ion-row>\n            <ion-col>\n                <ion-button color=\"primary\" expand=\"full\" (click)=\"skip = false;\">Elküld</ion-button>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>\n\n<ion-content *ngIf=\"customPerception && !skip\">\n  <app-patrol-custom-perception></app-patrol-custom-perception>\n  <ion-button color=\"primary\" *ngIf=\"customPerception\" (click)=\"customPerception = false; images = [];\">Elküld</ion-button>\n</ion-content>"

/***/ }),

/***/ "./src/app/patrol/guard/patrol-guard.page.scss":
/*!*****************************************************!*\
  !*** ./src/app/patrol/guard/patrol-guard.page.scss ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  height: 60px;\n  font-size: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9teW1hYy9Eb2N1bWVudHMvR2l0UHJvamVjdHMvRS1zYXMtSU9OSUMvc3JjL2FwcC9wYXRyb2wvZ3VhcmQvcGF0cm9sLWd1YXJkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQVc7RUFDWCxlQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYXRyb2wvZ3VhcmQvcGF0cm9sLWd1YXJkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xyXG4gICAgaGVpZ2h0OjYwcHg7XHJcbiAgICBmb250LXNpemU6MjBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/patrol/guard/patrol-guard.page.ts":
/*!***************************************************!*\
  !*** ./src/app/patrol/guard/patrol-guard.page.ts ***!
  \***************************************************/
/*! exports provided: PatrolGuardPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PatrolGuardPage", function() { return PatrolGuardPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _patrol_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../patrol.service */ "./src/app/patrol/patrol.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PatrolGuardPage = /** @class */ (function () {
    function PatrolGuardPage(_patrolService, _toastController, route) {
        this._patrolService = _patrolService;
        this._toastController = _toastController;
        this.route = route;
        this.skip = false;
        this.inProgress = false;
        this.finished = false;
        this.progress = 1;
        this.customPerception = false;
        this.patrolId = null;
        this.instanceId = null;
        this.loading = false;
        this.checkPoints = [];
    }
    PatrolGuardPage.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.patrolId = params['id'];
            _this.instanceId = params['instanceId'];
            _this._patrolService.getPatrolById(params['id']).subscribe(function (data) {
                _this.currentPatrol = data;
            }); // (+) converts string 'id' to a number
        });
    };
    PatrolGuardPage.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    PatrolGuardPage.prototype.startPatrol = function () {
        var _this = this;
        this.loading = true;
        if (this.currentPatrol.instances[0].status == 'PENDING') {
            this._patrolService.startPatrolInstance(this.instanceId).subscribe(function (data) {
                _this._patrolService.getPatrolInstanceById(_this.instanceId).subscribe(function (instance) {
                    _this.inProgress = true;
                    _this.checkPoints = instance.specificCheckInstances;
                    _this.loading = false;
                });
            });
        }
        else {
            this._patrolService.getPatrolInstanceById(this.instanceId).subscribe(function (instance) {
                _this.inProgress = true;
                _this.checkPoints = instance.specificCheckInstances;
                _this.loading = false;
            });
        }
    };
    PatrolGuardPage.prototype.onFinished = function () {
        var _this = this;
        if (this.checkPoints[this.progress]) {
            this.progress++;
        }
        else {
            this.loading = true;
            this._patrolService.finishPatrolInstance(this.instanceId).subscribe(function (data) {
                _this.finished = true;
                _this.loading = false;
            });
        }
    };
    PatrolGuardPage.prototype.onCheckpointStarted = function (event) {
        var _this = this;
        this.loading = true;
        this._patrolService.getPatrolInstanceById(this.instanceId).subscribe(function (instance) {
            _this.checkPoints = instance.specificCheckInstances;
            _this.loading = false;
        });
    };
    Object.defineProperty(PatrolGuardPage.prototype, "currentCheckpoint", {
        get: function () {
            return this.checkPoints[this.progress - 1];
        },
        enumerable: true,
        configurable: true
    });
    PatrolGuardPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-patrol-guard',
            template: __webpack_require__(/*! ./patrol-guard.page.html */ "./src/app/patrol/guard/patrol-guard.page.html"),
            styles: [__webpack_require__(/*! ./patrol-guard.page.scss */ "./src/app/patrol/guard/patrol-guard.page.scss")]
        }),
        __metadata("design:paramtypes", [_patrol_service__WEBPACK_IMPORTED_MODULE_2__["PatrolService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]])
    ], PatrolGuardPage);
    return PatrolGuardPage;
}());



/***/ })

}]);
//# sourceMappingURL=patrol-guard-patrol-guard-module.js.map