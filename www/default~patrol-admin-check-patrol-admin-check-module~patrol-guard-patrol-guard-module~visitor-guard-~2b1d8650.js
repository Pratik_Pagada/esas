(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~patrol-admin-check-patrol-admin-check-module~patrol-guard-patrol-guard-module~visitor-guard-~2b1d8650"],{

/***/ "./src/app/layout/layout.component.html":
/*!**********************************************!*\
  !*** ./src/app/layout/layout.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-grid>\r\n    <ion-row>\r\n        <ion-col>\r\n            <svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 1213 930\">\r\n                <g id=\"white background\">\r\n                    <path id=\"bal felső\" class=\"shp0\" d=\"M16,0v97h255.94c0,0 15.06,-5.64 15.06,-26c0,-20.36 0,-71 0,-71z\" (click)=\"setPlace('Courtyard 1')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 1')}\"/>\r\n                    <path id=\"middle left white\" class=\"shp0\" d=\"M426,352h391v-161h9v314h-265v26h-341l0,14h-35v-45l241,0z\" (click)=\"setPlace('Courtyard 3')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 3')}\"/>\r\n                    <path id=\"right white\" class=\"shp0\" d=\"M864,124v407v1h226c0,0 30.81,-1.36 40,30v319c0,0 1.9,29.18 15,35h68v-792z\" (click)=\"setPlace('Courtyard 4')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 4')}\"/>\r\n                    <path id=\"left white\" class=\"shp0\" d=\"M171,545v-45h-62v-392h-94v554v2v235h43v-235h49h2h14v-119z\" (click)=\"setPlace('Courtyard 5')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 5')}\"/>\r\n                    <path id=\"WH3 WH4 white\" class=\"shp0\" d=\"M657,544h434c0,0 25,8.34 25,17v314c0,0 1.28,41 -10,41h-69.07v-233h-196.93v-75h-183z\" (click)=\"setPlace('Courtyard 6')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 6')}\"/>\r\n                    <path id=\"bottom white\" class=\"shp0\" d=\"M57,886v12h-42c0,0 -0.33,10.59 21,18h1002v-30z\" (click)=\"setPlace('Courtyard 7')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 7')}\"/>\r\n                    <path id=\"upper white\" class=\"shp0\" d=\"M298,0v75c0,0 0.04,33 -27,33h-163v84h513v-30h45v-53h-180v-109z\" (click)=\"setPlace('Courtyard 2')\" [ngClass]=\"{'selected': isSelectedPlace('Courtyard 2')}\"/>\r\n                </g>\r\n                <g id=\"Alakzatok\">\r\n                    <path id=\"Parking lot right\" class=\"shp1\" d=\"M864,125h349v-125h-349z\" (click)=\"setPlace('Parking Lot 2')\" [ngClass]=\"{'selected': isSelectedPlace('Parking Lot 2')}\"/>\r\n                    <path id=\"Parking lot left\" class=\"shp1\" d=\"M485,110h202v-110h-202z\" (click)=\"setPlace('Parking Lot 1')\" [ngClass]=\"{'selected': isSelectedPlace('Parking Lot 1')}\"/>\r\n                    <path id=\"Gym\" class=\"shp2\" d=\"M777,63h64v-51h-64z\" (click)=\"setPlace('Gym')\" [ngClass]=\"{'selected': isSelectedPlace('Gym')}\"/>\r\n                    <path id=\"Reception\" class=\"shp3\" d=\"M781,122.11v69.89h-161l0,-30.83h45v-39.06z\" (click)=\"setPlace('B40')\" [ngClass]=\"{'selected': isSelectedPlace('B40')}\" />\r\n                    <path id=\"Office purple\" class=\"shp3\" d=\"M780,353h38v-162h-38z\" (click)=\"setPlace('B41')\" [ngClass]=\"{'selected': isSelectedPlace('B41')}\"/>\r\n                    <path id=\"Iroda M2 Office\" class=\"shp3\" d=\"M544,887h40v-343h-40z\" (click)=\"setPlace('M3')\" [ngClass]=\"{'selected': isSelectedPlace('M3')}\"/>\r\n                    <path id=\"Canteen\" class=\"shp4\" d=\"M780,192h62v-70h-62z\" (click)=\"setPlace('Canteen')\" [ngClass]=\"{'selected': isSelectedPlace('Canteen')}\"/>\r\n                    <path id=\"Baby Care\" class=\"shp5\" d=\"M767,202v150.98l-295.87,0l0,-130.98l-45.13,0v-20z\" (click)=\"setPlace('B30')\" [ngClass]=\"{'selected': isSelectedPlace('B30')}\"/>\r\n                    <path id=\"Office red\" class=\"shp6\" d=\"M426,353h46v-132h-46z\" (click)=\"setPlace('B20')\" [ngClass]=\"{'selected': isSelectedPlace('B20')}\"/>\r\n                    <path id=\"WH upper \" class=\"shp7\" d=\"M108,501h319v-310h-319zM170,545h16v-45h-16z\" (click)=\"setPlace('B10-11 WH')\" [ngClass]=\"{'selected': isSelectedPlace('B10-11 WH')}\"/>\r\n                    <path id=\"WH bottom left\" class=\"shp7\" d=\"M122,655h411v-111h-411z\" (click)=\"setPlace('M2-3 WH')\" [ngClass]=\"{'selected': isSelectedPlace('M2-3 WH')}\"/>\r\n                    <path id=\"WH bottom middle\" class=\"shp7\" d=\"M658,607h183v51h-246v-114h63z\" (click)=\"setPlace('M4 WH')\" [ngClass]=\"{'selected': isSelectedPlace('M4 WH')}\"/>\r\n                    <path id=\"WH bottom right\" class=\"shp7\" d=\"M840,887h198v-205h-198z\" (click)=\"setPlace('M5 WH')\" [ngClass]=\"{'selected': isSelectedPlace('M5 WH')}\"/>\r\n                    <path id=\"Molding\" class=\"shp8\" d=\"M122,887h190v-233h-190z\" (click)=\"setPlace('M1 Molding')\" [ngClass]=\"{'selected': isSelectedPlace('M1 Molding')}\"/>\r\n                    <path id=\"Unity\" class=\"shp4\" d=\"M57,887h66v-224h-66z\" (click)=\"setPlace('Unity')\" [ngClass]=\"{'selected': isSelectedPlace('Unity')}\"/>\r\n                    <path id=\"Oral-B\" class=\"shp9\" d=\"M311,887h222v-233h-222z\" (click)=\"setPlace('M2 Assemble')\" [ngClass]=\"{'selected': isSelectedPlace('M2 Assemble')}\"/>\r\n                    <path id=\"BRAUN\" class=\"shp10\" d=\"M595,887h246v-230h-246z\" (click)=\"setPlace('M4 Packing')\" [ngClass]=\"{'selected': isSelectedPlace('M4 Packing')}\"/>\r\n                    <path id=\"grey lines\" class=\"shp11\" d=\"M561,530v15h-342v-15h341l0,-1h11l0,1h278v-420h-163v-17h33v-39h-33v-16h33v-41h33v81h113l1,453c0,0 188.29,0 223,0c34.71,0 42,30 42,30c0,0 0,279.98 0,313c0,33.02 15,41 15,41h100v15c0,0 -1178.75,0 -1211,0c-32.25,0 -35,-31 -35,-31v-900h17v97c0,0 233.89,0 251,0c17.11,0 18,-26 18,-26v-70l0,0v-1h13v1c0,0 0,0.35 0,1c0,6.53 0,43.73 0,74c0,33.31 -29,34 -29,34h-254c0,0 0,772.18 0,787c0,14.82 21,19 21,19c0,0 1049.72,0 1066,0c16.28,0 12,-41 12,-41c0,0 0,-296.42 0,-310c0,-13.58 -25,-19 -25,-19h-520v-15z\" />\r\n                    <path id=\"PPE free area\" class=\"shp12\" d=\"M359,231v-40h422v162h-15v-150h-392v41h-266v-13zM826,191h10v325h-265v29h-11v-41h265c0,0 0,-293.89 0,-312.12c0,-0.58 0,-0.88 0,-0.88M532,545h0v-1h13v343h-13zM596,887h-13v-341l0,0v-2h13z\" />\r\n                </g>\r\n                <g id=\"Szöveg\">\r\n                    <text id=\"Parking lot 1\" style=\"transform: matrix(1,0,0,1,582.378,63.566)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt13\">Parkoló 1</tspan>\r\n                    </text>\r\n                    <text id=\"Parking lot 2\" style=\"transform: matrix(1,0,0,1,1056.181,72.017)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">Parkoló 2</tspan>\r\n                    </text>\r\n                    <text id=\"Reception\" style=\"transform: matrix(1,0,0,1,722.049,166.435)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">B40</tspan>\r\n                    </text>\r\n                    <text id=\"can teen\" style=\"transform: matrix(1,0,0,1,811.902,146.521)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt13\">étk.</tspan>\r\n                    </text>\r\n                    <text id=\"Baby care\" style=\"transform: matrix(1,0,0,1,627.428,290.096)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">B30</tspan>\r\n                    </text>\r\n                    <text id=\"WH 1\" style=\"transform: matrix(1,0,0,1,255.531,364.42)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">B10-11 Raktár</tspan>\r\n                    </text>\r\n                    <text id=\"WH 2\" style=\"transform: matrix(1,0,0,1,327.501,609.859)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">M2-3 Raktár</tspan>\r\n                    </text>\r\n                    <text id=\"WH 3\" style=\"transform: matrix(1,0,0,1,707.283,641.895)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">M4 Raktár</tspan>\r\n                    </text>\r\n                    <text id=\"WH 4\" style=\"transform: matrix(1,0,0,1,936.714,794.686)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">M5 Raktár</tspan>\r\n                    </text>\r\n                    <text id=\"BRAUN\" style=\"transform: matrix(1,0,0,1,718.802,792.974)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">M4 csomagolás</tspan>\r\n                    </text>\r\n                    <text id=\"Oral-B\" style=\"transform: matrix(1,0,0,1,424.127,794.947)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">M2 Összeszerelés</tspan>\r\n                    </text>\r\n                    <text id=\"Molding\" style=\"transform: matrix(1,0,0,1,211.179,795.101)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt15\">M1 Fröccsöntés</tspan>\r\n                    </text>\r\n                    <text id=\"Unity\" style=\"transform: matrix(0,-1.005,0.996,0,95.836,780.387)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">Unity</tspan>\r\n                    </text>\r\n                    <text id=\"purple office\" style=\"transform: matrix(0,-0.996,0.996,0,805.836,274)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">B41</tspan>\r\n                    </text>\r\n                    <text id=\"red office\" style=\"transform: matrix(0,-0.996,0.996,0,456.836,284)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">B20</tspan>\r\n                    </text>\r\n                    <text id=\"Iroda  M3  office\" style=\"transform: matrix(0,-1.002,0.996,0,572.836,713)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">Iroda  M3  office</tspan>\r\n                    </text>\r\n                    <text id=\"Gym\" style=\"transform: matrix(0.945,0,0,0.938,808.5,47.375)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">Gym</tspan>\r\n                    </text>\r\n                </g>\r\n                <path id=\"Office red másolat\" class=\"shp6\" d=\"M430,494h46v-66h-46z\" (click)=\"setPlace('Utility')\" [ngClass]=\"{'selected': isSelectedPlace('Utility')}\" />\r\n                <path id=\"Téglalap 1 másolat\" class=\"shp6\" d=\"M883,139h119v58h-119z\" (click)=\"setPlace('Apprenticeship building')\" [ngClass]=\"{'selected': isSelectedPlace('Apprenticeship building')}\" />\r\n                <text id=\"Tanműhely\" style=\"transform: matrix(1,0,0,1,948.316,228.125)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">Tanműhely</tspan>\r\n                </text>\r\n                <text id=\"Utility  building\" style=\"transform: matrix(0.999,0,0,0.998,531.045,416.885)\" >\r\n                    <tspan x=\"0\" y=\"0\" class=\"txt14\">Utility </tspan>\r\n                    <!--<tspan x=\"0\" y=\"33.6\" class=\"txt14\">building</tspan>-->\r\n                </text>\r\n                </svg>\r\n        </ion-col>\r\n    </ion-row>\r\n    <ion-row>\r\n        <ion-col>\r\n            <ion-select placeholder=\"Helység\" [attr.multiple]=\"multi ? '' : null\" [ngModel]=\"selectedPlaces\" (ngModelChange)=\"placeChange($event)\">\r\n                <ion-select-option *ngFor=\"let place of places\" value=\"{{place.id}}\">{{ place.englishName }}</ion-select-option>\r\n            </ion-select>\r\n        </ion-col>\r\n    </ion-row>\r\n</ion-grid>"

/***/ }),

/***/ "./src/app/layout/layout.component.scss":
/*!**********************************************!*\
  !*** ./src/app/layout/layout.component.scss ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "svg {\n  max-height: 500px; }\n  svg .txt13 {\n    font-size: 30px;\n    fill: #010101;\n    font-family: \"Times New Roman\";\n    text-anchor: middle; }\n  svg .txt14 {\n    font-size: 30px;\n    fill: black;\n    font-family: \"Times New Roman\";\n    text-anchor: middle; }\n  svg .txt15 {\n    font-size: 26px;\n    fill: black;\n    font-family: \"Times New Roman\";\n    text-anchor: middle; }\n  svg path.shp0 {\n    fill: #ffffff;\n    stroke: #000000; }\n  svg path.shp0:not(.inactive):hover, svg path.shp0.hovered, svg path.shp0.selected {\n      fill: #cccccc; }\n  svg path.shp0:not(.inactive).shp9:hover, svg path.shp0.shp9.hovered, svg path.shp0.shp9.selected {\n      fill: #666666; }\n  svg path.shp0.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp1 {\n    fill: #91cf4f;\n    stroke: #000000; }\n  svg path.shp1:not(.inactive):hover, svg path.shp1.hovered, svg path.shp1.selected {\n      fill: #5e9127; }\n  svg path.shp1:not(.inactive).shp9:hover, svg path.shp1.shp9.hovered, svg path.shp1.shp9.selected {\n      fill: black; }\n  svg path.shp1.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp2 {\n    fill: #5b9bd5;\n    stroke: #000000; }\n  svg path.shp2:not(.inactive):hover, svg path.shp2.hovered, svg path.shp2.selected {\n      fill: #2968a1; }\n  svg path.shp2:not(.inactive).shp9:hover, svg path.shp2.shp9.hovered, svg path.shp2.shp9.selected {\n      fill: black; }\n  svg path.shp2.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp3 {\n    fill: #6f2f9f;\n    stroke: #000000; }\n  svg path.shp3:not(.inactive):hover, svg path.shp3.hovered, svg path.shp3.selected {\n      fill: #381850; }\n  svg path.shp3:not(.inactive).shp9:hover, svg path.shp3.shp9.hovered, svg path.shp3.shp9.selected {\n      fill: black; }\n  svg path.shp3.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp4 {\n    fill: #ffff00;\n    stroke: #000000; }\n  svg path.shp4:not(.inactive):hover, svg path.shp4.hovered, svg path.shp4.selected {\n      fill: #999900; }\n  svg path.shp4:not(.inactive).shp9:hover, svg path.shp4.shp9.hovered, svg path.shp4.shp9.selected {\n      fill: black; }\n  svg path.shp4.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp5 {\n    fill: #00afef;\n    stroke: #000000; }\n  svg path.shp5:not(.inactive):hover, svg path.shp5.hovered, svg path.shp5.selected {\n      fill: #006489; }\n  svg path.shp5:not(.inactive).shp9:hover, svg path.shp5.shp9.hovered, svg path.shp5.shp9.selected {\n      fill: black; }\n  svg path.shp5.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp6 {\n    fill: #ff0000;\n    stroke: #000000; }\n  svg path.shp6:not(.inactive):hover, svg path.shp6.hovered, svg path.shp6.selected {\n      fill: #990000; }\n  svg path.shp6:not(.inactive).shp9:hover, svg path.shp6.shp9.hovered, svg path.shp6.shp9.selected {\n      fill: black; }\n  svg path.shp6.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp7 {\n    fill: #ffbf00;\n    stroke: #000000; }\n  svg path.shp7:not(.inactive):hover, svg path.shp7.hovered, svg path.shp7.selected {\n      fill: #997300; }\n  svg path.shp7:not(.inactive).shp9:hover, svg path.shp7.shp9.hovered, svg path.shp7.shp9.selected {\n      fill: black; }\n  svg path.shp7.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp8 {\n    fill: #d5dbe5;\n    stroke: #000000; }\n  svg path.shp8:not(.inactive):hover, svg path.shp8.hovered, svg path.shp8.selected {\n      fill: #96a5be; }\n  svg path.shp8:not(.inactive).shp9:hover, svg path.shp8.shp9.hovered, svg path.shp8.shp9.selected {\n      fill: #344054; }\n  svg path.shp8.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp9 {\n    fill: #ffe599;\n    stroke: #000000; }\n  svg path.shp9:not(.inactive):hover, svg path.shp9.hovered, svg path.shp9.selected {\n      fill: #ffcb33; }\n  svg path.shp9:not(.inactive).shp9:hover, svg path.shp9.shp9.hovered, svg path.shp9.shp9.selected {\n      fill: #664c00; }\n  svg path.shp9.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp10 {\n    fill: #a9d18d;\n    stroke: #000000; }\n  svg path.shp10:not(.inactive):hover, svg path.shp10.hovered, svg path.shp10.selected {\n      fill: #73b147; }\n  svg path.shp10:not(.inactive).shp9:hover, svg path.shp10.shp9.hovered, svg path.shp10.shp9.selected {\n      fill: #141f0d; }\n  svg path.shp10.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp11 {\n    fill: #a5a5a5;\n    stroke: #000000;\n    pointer-events: none; }\n  svg path.shp11:not(.inactive):hover, svg path.shp11.hovered, svg path.shp11.selected {\n      fill: #727272; }\n  svg path.shp11:not(.inactive).shp9:hover, svg path.shp11.shp9.hovered, svg path.shp11.shp9.selected {\n      fill: #0c0c0c; }\n  svg path.shp11.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path.shp12 {\n    fill: #6fad47;\n    stroke: #000000;\n    pointer-events: none; }\n  svg path.shp12:not(.inactive):hover, svg path.shp12.hovered, svg path.shp12.selected {\n      fill: #416529; }\n  svg path.shp12:not(.inactive).shp9:hover, svg path.shp12.shp9.hovered, svg path.shp12.shp9.selected {\n      fill: black; }\n  svg path.shp12.selected {\n      stroke: red;\n      stroke-width: 10px; }\n  svg path:hover {\n    cursor: pointer; }\n  svg text {\n    font-weight: bold;\n    pointer-events: none; }\n  svg tspan {\n    white-space: pre; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9teW1hYy9Eb2N1bWVudHMvR2l0UHJvamVjdHMvRS1zYXMtSU9OSUMvc3JjL2FwcC9sYXlvdXQvbGF5b3V0LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWdCQTtFQUNJLGlCQUFnQixFQUFBO0VBRHBCO0lBRWEsZUFBZTtJQUFDLGFBQWdCO0lBQUMsOEJBQThCO0lBQUMsbUJBQW9CLEVBQUE7RUFGakc7SUFHYSxlQUFlO0lBQUMsV0FBZ0I7SUFBQyw4QkFBOEI7SUFBQyxtQkFBb0IsRUFBQTtFQUhqRztJQUlhLGVBQWU7SUFBQyxXQUFnQjtJQUFDLDhCQUE4QjtJQUFDLG1CQUFvQixFQUFBO0VBSmpHO0lBZkksYUFzQnVDO0lBQUUsZUFBZ0IsRUFBQTtFQXJCekQ7TUFDSSxhQUEyQixFQUFBO0VBRy9CO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQVc7TUFDWCxrQkFBaUIsRUFBQTtFQUl6QjtJQWZJLGFBdUJ1QztJQUFFLGVBQWdCLEVBQUE7RUF0QnpEO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUFXO01BQ1gsa0JBQWlCLEVBQUE7RUFJekI7SUFmSSxhQXdCdUM7SUFBRSxlQUFnQixFQUFBO0VBdkJ6RDtNQUNJLGFBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUEyQixFQUFBO0VBRy9CO01BQ0ksV0FBVztNQUNYLGtCQUFpQixFQUFBO0VBSXpCO0lBZkksYUF5QnVDO0lBQUUsZUFBZ0IsRUFBQTtFQXhCekQ7TUFDSSxhQUEyQixFQUFBO0VBRy9CO01BQ0ksV0FBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQVc7TUFDWCxrQkFBaUIsRUFBQTtFQUl6QjtJQWZJLGFBMEJ1QztJQUFFLGVBQWdCLEVBQUE7RUF6QnpEO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUFXO01BQ1gsa0JBQWlCLEVBQUE7RUFJekI7SUFmSSxhQTJCdUM7SUFBRSxlQUFnQixFQUFBO0VBMUJ6RDtNQUNJLGFBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUEyQixFQUFBO0VBRy9CO01BQ0ksV0FBVztNQUNYLGtCQUFpQixFQUFBO0VBSXpCO0lBZkksYUE0QnVDO0lBQUUsZUFBZ0IsRUFBQTtFQTNCekQ7TUFDSSxhQUEyQixFQUFBO0VBRy9CO01BQ0ksV0FBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQVc7TUFDWCxrQkFBaUIsRUFBQTtFQUl6QjtJQWZJLGFBNkJ1QztJQUFFLGVBQWdCLEVBQUE7RUE1QnpEO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUFXO01BQ1gsa0JBQWlCLEVBQUE7RUFJekI7SUFmSSxhQThCdUM7SUFBRSxlQUFnQixFQUFBO0VBN0J6RDtNQUNJLGFBQTJCLEVBQUE7RUFHL0I7TUFDSSxhQUEyQixFQUFBO0VBRy9CO01BQ0ksV0FBVztNQUNYLGtCQUFpQixFQUFBO0VBSXpCO0lBZkksYUErQnVDO0lBQUUsZUFBZ0IsRUFBQTtFQTlCekQ7TUFDSSxhQUEyQixFQUFBO0VBRy9CO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQVc7TUFDWCxrQkFBaUIsRUFBQTtFQUl6QjtJQWZJLGFBZ0N3QztJQUFFLGVBQWdCLEVBQUE7RUEvQjFEO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLGFBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUFXO01BQ1gsa0JBQWlCLEVBQUE7RUFJekI7SUFmSSxhQWlDd0M7SUFBRSxlQUFlO0lBQUUsb0JBQW9CLEVBQUE7RUFoQy9FO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLGFBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUFXO01BQ1gsa0JBQWlCLEVBQUE7RUFJekI7SUFmSSxhQWtDd0M7SUFBRSxlQUFlO0lBQUMsb0JBQW9CLEVBQUE7RUFqQzlFO01BQ0ksYUFBMkIsRUFBQTtFQUcvQjtNQUNJLFdBQTJCLEVBQUE7RUFHL0I7TUFDSSxXQUFXO01BQ1gsa0JBQWlCLEVBQUE7RUFJekI7SUF1QlksZUFBYyxFQUFBO0VBdkIxQjtJQTRCUSxpQkFBaUI7SUFDakIsb0JBQW9CLEVBQUE7RUE3QjVCO0lBZ0NZLGdCQUFnQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvbGF5b3V0L2xheW91dC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBtaXhpbiBiZy1jb2xvcigkYmdjb2xvcjogbGlnaHRzZWFncmVlbikgeyBcclxuICAgIGZpbGw6ICRiZ2NvbG9yOyBcclxuICAgICY6bm90KC5pbmFjdGl2ZSk6aG92ZXIsICYuaG92ZXJlZCwgJi5zZWxlY3RlZCB7IFxyXG4gICAgICAgIGZpbGw6IGRhcmtlbigkYmdjb2xvciwgMjAlKTsgXHJcbiAgICB9IFxyXG5cclxuICAgICY6bm90KC5pbmFjdGl2ZSkuc2hwOTpob3ZlciwgJi5zaHA5LmhvdmVyZWQsICYuc2hwOS5zZWxlY3RlZCB7XHJcbiAgICAgICAgZmlsbDogZGFya2VuKCRiZ2NvbG9yLCA2MCUpOyBcclxuICAgIH1cclxuIFxyXG4gICAgJi5zZWxlY3RlZCB7IFxyXG4gICAgICAgIHN0cm9rZTogcmVkOyBcclxuICAgICAgICBzdHJva2Utd2lkdGg6MTBweDsgXHJcbiAgICB9IFxyXG59IFxyXG4gXHJcbnN2ZyB7IFxyXG4gICAgbWF4LWhlaWdodDo1MDBweDsgXHJcbiAgICAudHh0MTMgeyBmb250LXNpemU6IDMwcHg7ZmlsbDogcmdiKDEsMSwxKTtmb250LWZhbWlseTogXCJUaW1lcyBOZXcgUm9tYW5cIjt0ZXh0LWFuY2hvcjogbWlkZGxlIH0gIFxyXG4gICAgLnR4dDE0IHsgZm9udC1zaXplOiAzMHB4O2ZpbGw6IHJnYigwLDAsMCk7Zm9udC1mYW1pbHk6IFwiVGltZXMgTmV3IFJvbWFuXCI7dGV4dC1hbmNob3I6IG1pZGRsZSB9ICBcclxuICAgIC50eHQxNSB7IGZvbnQtc2l6ZTogMjZweDtmaWxsOiByZ2IoMCwwLDApO2ZvbnQtZmFtaWx5OiBcIlRpbWVzIE5ldyBSb21hblwiO3RleHQtYW5jaG9yOiBtaWRkbGUgfSAgXHJcbiBcclxuICAgIHBhdGggeyBcclxuICAgICAgICAmLnNocDAgeyBAaW5jbHVkZSBiZy1jb2xvciggI2ZmZmZmZik7c3Ryb2tlOiAjMDAwMDAwIH0gIFxyXG4gICAgICAgICYuc2hwMSB7IEBpbmNsdWRlIGJnLWNvbG9yKCAjOTFjZjRmKTtzdHJva2U6ICMwMDAwMDAgfSAgXHJcbiAgICAgICAgJi5zaHAyIHsgQGluY2x1ZGUgYmctY29sb3IoICM1YjliZDUpO3N0cm9rZTogIzAwMDAwMCB9ICBcclxuICAgICAgICAmLnNocDMgeyBAaW5jbHVkZSBiZy1jb2xvciggIzZmMmY5Zik7c3Ryb2tlOiAjMDAwMDAwIH0gIFxyXG4gICAgICAgICYuc2hwNCB7IEBpbmNsdWRlIGJnLWNvbG9yKCAjZmZmZjAwKTtzdHJva2U6ICMwMDAwMDAgfSAgXHJcbiAgICAgICAgJi5zaHA1IHsgQGluY2x1ZGUgYmctY29sb3IoICMwMGFmZWYpO3N0cm9rZTogIzAwMDAwMCB9ICBcclxuICAgICAgICAmLnNocDYgeyBAaW5jbHVkZSBiZy1jb2xvciggI2ZmMDAwMCk7c3Ryb2tlOiAjMDAwMDAwIH0gIFxyXG4gICAgICAgICYuc2hwNyB7IEBpbmNsdWRlIGJnLWNvbG9yKCAjZmZiZjAwKTtzdHJva2U6ICMwMDAwMDAgfSAgXHJcbiAgICAgICAgJi5zaHA4IHsgQGluY2x1ZGUgYmctY29sb3IoICNkNWRiZTUpO3N0cm9rZTogIzAwMDAwMCB9ICBcclxuICAgICAgICAmLnNocDkgeyBAaW5jbHVkZSBiZy1jb2xvciggI2ZmZTU5OSk7c3Ryb2tlOiAjMDAwMDAwIH0gIFxyXG4gICAgICAgICYuc2hwMTAgeyBAaW5jbHVkZSBiZy1jb2xvciggI2E5ZDE4ZCk7c3Ryb2tlOiAjMDAwMDAwIH0gIFxyXG4gICAgICAgICYuc2hwMTEgeyBAaW5jbHVkZSBiZy1jb2xvciggI2E1YTVhNSk7c3Ryb2tlOiAjMDAwMDAwOyBwb2ludGVyLWV2ZW50czogbm9uZTsgfSAgXHJcbiAgICAgICAgJi5zaHAxMiB7IEBpbmNsdWRlIGJnLWNvbG9yKCAjNmZhZDQ3KTtzdHJva2U6ICMwMDAwMDA7cG9pbnRlci1ldmVudHM6IG5vbmU7IH0gIFxyXG4gICAgICAgICBcclxuIFxyXG4gICAgICAgICY6aG92ZXIgeyBcclxuICAgICAgICAgICAgY3Vyc29yOnBvaW50ZXI7IFxyXG4gICAgICAgIH0gXHJcbiAgICB9IFxyXG4gXHJcbiAgICB0ZXh0IHsgXHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7IFxyXG4gICAgICAgIHBvaW50ZXItZXZlbnRzOiBub25lOyBcclxuICAgIH0gXHJcbiBcclxuICAgIHRzcGFuIHsgd2hpdGUtc3BhY2U6cHJlIH0gXHJcbiBcclxufSAiXX0= */"

/***/ }),

/***/ "./src/app/layout/layout.component.ts":
/*!********************************************!*\
  !*** ./src/app/layout/layout.component.ts ***!
  \********************************************/
/*! exports provided: LayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponent", function() { return LayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/layout.service */ "./src/app/services/layout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LayoutComponent = /** @class */ (function () {
    function LayoutComponent(_layoutService) {
        this._layoutService = _layoutService;
        this.placeSelected = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.places = [];
    }
    LayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._layoutService.placeSubscription.subscribe(function (data) {
            _this.places = data;
        });
        this.refreshPlaces();
        if (this.multi) {
            this.selectedPlaces = [];
            if (this.default) {
                for (var i = 0, len = this.places.length; i < len; i++) {
                    if (this.places[i].id === this.default.id) {
                        this.selectedPlaces.push(this.places[i].id);
                    }
                }
            }
        }
        else {
            this.selectedPlaces = '';
            if (this.default) {
                for (var i = 0, len = this.places.length; i < len; i++) {
                    if (this.places[i].id == this.default) {
                        this.selectedPlaces = this.places[i].id;
                    }
                }
            }
        }
    };
    LayoutComponent.prototype.refreshPlaces = function () {
        this._layoutService.refreshPlaces();
    };
    LayoutComponent.prototype.placeChange = function (newValue) {
        this.selectedPlaces = newValue;
        this.placeSelected.emit(this.selectedPlaces);
    };
    LayoutComponent.prototype.isSelectedPlace = function (placeName) {
        if (this.multi) {
            var arr = this.selectedPlaces;
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].englishName === placeName) {
                    return true;
                }
            }
        }
        else {
            for (var i = 0, len = this.places.length; i < len; i++) {
                if (this.places[i].englishName === placeName) {
                    return this.selectedPlaces === this.places[i].id;
                }
            }
        }
        //return this.newKeyForm.value.place.englishName === placeName;
    };
    LayoutComponent.prototype.setPlace = function (placeName) {
        if (this.multi) {
            for (var i = 0, len = this.places.length; i < len; i++) {
                if (this.places[i].englishName === placeName) {
                    var arr = this.selectedPlaces;
                    var index = arr.indexOf(this.places[i].id);
                    if (index > -1) {
                        arr.splice(index, 1);
                    }
                    else {
                        arr.push(this.places[i].id);
                    }
                    this.selectedPlaces = [];
                    this.selectedPlaces = arr;
                    break;
                }
            }
            this.placeSelected.emit(this.selectedPlaces);
        }
        else {
            for (var i = 0, len = this.places.length; i < len; i++) {
                if (this.selectedPlaces === this.places[i].id) {
                    this.selectedPlaces = '';
                    this.placeSelected.emit(this.selectedPlaces);
                    return;
                }
                else if (this.places[i].englishName === placeName) {
                    this.selectedPlaces = this.places[i].id;
                    this.placeSelected.emit(this.selectedPlaces);
                    return;
                }
            }
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", Object)
    ], LayoutComponent.prototype, "placeSelected", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], LayoutComponent.prototype, "multi", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], LayoutComponent.prototype, "default", void 0);
    LayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-layout',
            template: __webpack_require__(/*! ./layout.component.html */ "./src/app/layout/layout.component.html"),
            styles: [__webpack_require__(/*! ./layout.component.scss */ "./src/app/layout/layout.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_layout_service__WEBPACK_IMPORTED_MODULE_1__["LayoutService"]])
    ], LayoutComponent);
    return LayoutComponent;
}());



/***/ }),

/***/ "./src/app/layout/layout.module.ts":
/*!*****************************************!*\
  !*** ./src/app/layout/layout.module.ts ***!
  \*****************************************/
/*! exports provided: LayoutComponentModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutComponentModule", function() { return LayoutComponentModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _layout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./layout.component */ "./src/app/layout/layout.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var LayoutComponentModule = /** @class */ (function () {
    function LayoutComponentModule() {
    }
    LayoutComponentModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"]
            ],
            declarations: [_layout_component__WEBPACK_IMPORTED_MODULE_2__["LayoutComponent"]],
            exports: [_layout_component__WEBPACK_IMPORTED_MODULE_2__["LayoutComponent"]]
        })
    ], LayoutComponentModule);
    return LayoutComponentModule;
}());



/***/ })

}]);
//# sourceMappingURL=default~patrol-admin-check-patrol-admin-check-module~patrol-guard-patrol-guard-module~visitor-guard-~2b1d8650.js.map