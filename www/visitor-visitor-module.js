(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["visitor-visitor-module"],{

/***/ "./src/app/visitor/visitor.module.ts":
/*!*******************************************!*\
  !*** ./src/app/visitor/visitor.module.ts ***!
  \*******************************************/
/*! exports provided: VisitorPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorPageModule", function() { return VisitorPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _visitor_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./visitor.page */ "./src/app/visitor/visitor.page.ts");
/* harmony import */ var _header_header_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../header/header.module */ "./src/app/header/header.module.ts");
/* harmony import */ var _layout_layout_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../layout/layout.module */ "./src/app/layout/layout.module.ts");
/* harmony import */ var _visitor_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./visitor.service */ "./src/app/visitor/visitor.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var VisitorPageModule = /** @class */ (function () {
    function VisitorPageModule() {
    }
    VisitorPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _header_header_module__WEBPACK_IMPORTED_MODULE_6__["HeaderModule"],
                _layout_layout_module__WEBPACK_IMPORTED_MODULE_7__["LayoutComponentModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _visitor_page__WEBPACK_IMPORTED_MODULE_5__["VisitorPage"]
                    }
                ])
            ],
            declarations: [_visitor_page__WEBPACK_IMPORTED_MODULE_5__["VisitorPage"]],
            providers: [_visitor_service__WEBPACK_IMPORTED_MODULE_8__["VisitorService"]]
        })
    ], VisitorPageModule);
    return VisitorPageModule;
}());



/***/ }),

/***/ "./src/app/visitor/visitor.page.html":
/*!*******************************************!*\
  !*** ./src/app/visitor/visitor.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header [headerText]=\"'Visitor reporting'\"></app-header>\n\n<ion-content>\n  <form [formGroup]=\"visitorForm\">\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n            <app-layout (placeSelected)='refreshSelectedPlaces($event)'></app-layout>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-input type=\"text\" formControlName=\"access\" placeholder=\"Beléptető jogok\"></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\" size-md>\n          <ion-select placeholder=\"Fogadó személy\" formControlName=\"host\">\n            <ion-select-option *ngFor=\"let user of users.content\" [value]=\"user\">{{ user.firstName }} {{ user.lastName }}</ion-select-option>\n          </ion-select>\n        </ion-col>\n        <ion-col size=\"12\" size-md>\n          <ion-input type=\"text\" formControlName=\"phoneNumber\" placeholder=\"Fogadó személy telefonszáma\"></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-select formControlName=\"notificationType\" placeholder=\"Értesítés módja\">\n            <ion-select-option value=\"phone\">Értesítés telefonon</ion-select-option>\n            <ion-select-option value=\"app\">Értesítés applikáción keresztül</ion-select-option>\n          </ion-select>\n        </ion-col>\n      </ion-row>\n      <ion-row formArrayName=\"visitors\" *ngFor=\"let visitor of visitors.controls; let visitorIndex=index\" >\n          <ion-col size=\"12\" size-md formGroupName=\"{{visitorIndex}}\">\n            <ion-input type=\"text\" placeholder=\"Látogató neve\"  formControlName=\"name\"></ion-input>\n          </ion-col>\n          <ion-col *ngIf=\"visitorIndex == 0\" size=\"12\" size-md formGroupName=\"{{visitorIndex}}\">\n                <ion-input type=\"text\" placeholder=\"Látogató e-mail címe\" formControlName=\"email\"></ion-input>\n          </ion-col> \n          <ion-col *ngIf=\"visitorIndex != 0\" size=\"10\" size-md formGroupName=\"{{visitorIndex}}\">\n              <ion-input type=\"text\" placeholder=\"Látogató e-mail címe\" formControlName=\"email\"></ion-input>\n          </ion-col>\n          <ion-col *ngIf=\"visitorIndex != 0\" size=\"2\" size-md>\n              <ion-icon style=\"color:red\" size=\"large\" item-right name=\"remove\"  (click)=\"removeItem(visitorIndex)\"></ion-icon>\n          </ion-col>\n      </ion-row>\n      <ion-row>\n          <ion-col>\n              <ion-icon style=\"color:green\" size=\"large\" name=\"add\"  (click)=\"addItem()\"></ion-icon>\n          </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col size=\"12\" size-md>\n            <ion-datetime formControlName=\"from\" placeholder=\"Érkezés\" displayFormat=\"YYYY-MM-DD HH:mm\" pickerFormat=\"YYYY-MM-DD HH:mm\"></ion-datetime>\n        </ion-col>\n        <ion-col size=\"12\" size-md>\n            <ion-datetime formControlName=\"to\" placeholder=\"Távozás\"  displayFormat=\"YYYY-MM-DD HH:mm\" pickerFormat=\"YYYY-MM-DD HH:mm\"></ion-datetime>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-input type=\"text\" placeholder=\"Látogatás oka\" formControlName=\"reason\"></ion-input>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n              <ion-checkbox formControlName=\"otherRequest\"></ion-checkbox>\n              <ion-label style=\"padding-left:5px\" >Egyéb kérés</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"visitorForm.value.otherRequest\">\n          <ion-input type=\"text\" placeholder=\"Egyéb kérés\" formControlName=\"otherRequestDescription\"></ion-input>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <ion-button expand=\"full\" (click)=\"submit()\">Elküld</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/visitor/visitor.page.scss":
/*!*******************************************!*\
  !*** ./src/app/visitor/visitor.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Zpc2l0b3IvdmlzaXRvci5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/visitor/visitor.page.ts":
/*!*****************************************!*\
  !*** ./src/app/visitor/visitor.page.ts ***!
  \*****************************************/
/*! exports provided: VisitorPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorPage", function() { return VisitorPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _visitor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./visitor.service */ "./src/app/visitor/visitor.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var VisitorPage = /** @class */ (function () {
    function VisitorPage(_visitorService, _toastController) {
        this._visitorService = _visitorService;
        this._toastController = _toastController;
        this.users = {
            content: []
        };
    }
    VisitorPage.prototype.ngOnInit = function () {
        this.refreshUsers();
        this.visitorForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            place: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]([], [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            wholeEstablishment: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false),
            access: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            host: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            phoneNumber: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            from: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            to: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            reason: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            visitors: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormArray"]([
                new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
                    name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
                    email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', null)
                })
            ]),
            otherRequest: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](false),
            otherRequestDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            notificationType: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('phone')
        });
    };
    VisitorPage.prototype.submit = function () {
        var _this = this;
        var data = {
            "placeId": this.visitorForm.value.place[0].id,
            "hostId": this.visitorForm.value.host.id,
            "phoneNumber": this.visitorForm.value.phoneNumber,
            "from": this.visitorForm.value.from,
            "to": this.visitorForm.value.to,
            "reason": this.visitorForm.value.reason,
            "visitors": this.visitorForm.value.visitors,
            "otherRequest": this.visitorForm.value.otherRequest,
            "otherRequestDescription": this.visitorForm.value.otherRequestDescription,
            "notificationType": this.visitorForm.value.notificationType,
            "access": this.visitorForm.value.access
        };
        this._visitorService.submit(data).subscribe(function (data) {
            _this.openSuccessSnack();
        }, function (error) {
            _this.openErrorSnack();
        });
    };
    VisitorPage.prototype.openSuccessSnack = function () {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._toastController.create({
                            message: 'Művelet sikeres.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    VisitorPage.prototype.openErrorSnack = function () {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this._toastController.create({
                            message: 'Művelet sikertelen, háttérrendszer hiba!',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    VisitorPage.prototype.refreshUsers = function () {
        var _this = this;
        this._visitorService.getAllUsers().subscribe(function (data) {
            _this.users = data;
        });
    };
    Object.defineProperty(VisitorPage.prototype, "visitors", {
        get: function () {
            return this.visitorForm.get('visitors');
        },
        enumerable: true,
        configurable: true
    });
    ;
    VisitorPage.prototype.removeItem = function (index) {
        this.visitors.removeAt(index);
    };
    VisitorPage.prototype.addItem = function () {
        this.visitors.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', null)
        }));
    };
    VisitorPage.prototype.refreshSelectedPlaces = function (places) {
        this.visitorForm.controls['place'].setValue(places);
    };
    VisitorPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-visitor',
            template: __webpack_require__(/*! ./visitor.page.html */ "./src/app/visitor/visitor.page.html"),
            styles: [__webpack_require__(/*! ./visitor.page.scss */ "./src/app/visitor/visitor.page.scss")]
        }),
        __metadata("design:paramtypes", [_visitor_service__WEBPACK_IMPORTED_MODULE_2__["VisitorService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]])
    ], VisitorPage);
    return VisitorPage;
}());



/***/ })

}]);
//# sourceMappingURL=visitor-visitor-module.js.map