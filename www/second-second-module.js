(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["second-second-module"],{

/***/ "./src/app/second/second.module.ts":
/*!*****************************************!*\
  !*** ./src/app/second/second.module.ts ***!
  \*****************************************/
/*! exports provided: SecondPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecondPageModule", function() { return SecondPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _second_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./second.page */ "./src/app/second/second.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _second_page__WEBPACK_IMPORTED_MODULE_5__["SecondPage"]
    }
];
var SecondPageModule = /** @class */ (function () {
    function SecondPageModule() {
    }
    SecondPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_second_page__WEBPACK_IMPORTED_MODULE_5__["SecondPage"]]
        })
    ], SecondPageModule);
    return SecondPageModule;
}());



/***/ }),

/***/ "./src/app/second/second.page.html":
/*!*****************************************!*\
  !*** ./src/app/second/second.page.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-card>\n  <ion-card-header>\n    <ion-card-subtitle>Vendége érkezett!</ion-card-subtitle>\n    <ion-card-title>{{name}}</ion-card-title>\n  </ion-card-header>\n  <ion-card-content>\n        <ion-item class=\"activated\">\n          <ion-icon name=\"calendar\" slot=\"start\"></ion-icon>\n          <ion-label>{{from}}</ion-label>\n        </ion-item>\n        <ion-item>\n          <ion-icon name=\"card\" slot=\"start\"></ion-icon>\n          <ion-label>{{businessReason}}</ion-label>\n        </ion-item>\n        <div *ngFor=\"let visitor of visitors\">\n            <ion-item class=\"activated\">\n                <ion-icon name=\"person\" slot=\"start\"></ion-icon>\n                <ion-label>{{visitor.name}}</ion-label>\n              </ion-item>    \n\n            <!-- <ion-item class=\"activated\">\n                <ion-icon name=\"mail\" slot=\"start\"></ion-icon>\n                <ion-label>{{visitor.email}}</ion-label>\n              </ion-item> -->\n        </div>\n        \n        <!--\n        <ion-item>\n          <ion-icon name=\"home\" slot=\"start\"></ion-icon>\n          <ion-label>{{visitor_company}}</ion-label>\n        </ion-item>\n        -->\n        \n      \n        \n      \n        <ion-button color=\"primary\" expand=\"full\" (click)=\"answer('Indulok!')\">Indulok!</ion-button>\n        <ion-button color=\"primary\" expand=\"full\" (click)=\"answer('Kérem várjon!')\">Kérem, várjon!</ion-button>\n        <ion-button color=\"primary\" expand=\"full\" (click)=\"answer('Most nem alkalmas!')\">Most nem alkalmas!</ion-button>\n  </ion-card-content>\n</ion-card>"

/***/ }),

/***/ "./src/app/second/second.page.scss":
/*!*****************************************!*\
  !*** ./src/app/second/second.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NlY29uZC9zZWNvbmQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/second/second.page.ts":
/*!***************************************!*\
  !*** ./src/app/second/second.page.ts ***!
  \***************************************/
/*! exports provided: SecondPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecondPage", function() { return SecondPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _visitor_visitor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../visitor/visitor.service */ "./src/app/visitor/visitor.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SecondPage = /** @class */ (function () {
    // visitor: object = {
    //   v_name: '',
    //   email: '',
    //   company: '',
    //   arrivaldate: '',
    //   cardnumber: '',
    //   img: ''
    // };
    function SecondPage(route, router, visitorService) {
        this.route = route;
        this.router = router;
        this.visitorService = visitorService;
        this.info = '';
        this.name = '';
        this.email = '';
        this.message = '';
        this.visitor_name = '';
        this.visitor_email = '';
        this.visitor_company = '';
        this.visitor_date = '';
        this.visitor_cardnumber = '';
        this.businessReason = '';
        this.from = '';
        this.img = 'https://pbs.twimg.com/profile_images/858987821394210817/oMccbXv6_bigger.jpg';
        this.info = this.route.snapshot.params['price'];
        this.data = JSON.parse(this.info);
        this.name = this.data['name'];
        this.email = this.data['email'];
        this.message = this.data['message'];
        this.visitors = this.data['visitors'];
        this.businessReason = this.data['businessReason'];
        this.from = this.data['from'];
        this.visitor_name = this.visitors.name;
        this.visitor_email = this.visitors.email;
        this.visitor_company = this.visitors.company;
        this.visitor_date = this.visitors.arrivalDate;
        this.visitor_cardnumber = this.visitors.cardnumber;
    }
    SecondPage.prototype.ngOnInit = function () {
    };
    SecondPage.prototype.addVisitor = function () {
    };
    SecondPage.prototype.answer = function (message) {
        var _this = this;
        var data = {
            message: message
        };
        this.visitorService.answer(data).subscribe(function (data) {
            _this.router.navigate(['/']);
        });
    };
    SecondPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-second',
            template: __webpack_require__(/*! ./second.page.html */ "./src/app/second/second.page.html"),
            styles: [__webpack_require__(/*! ./second.page.scss */ "./src/app/second/second.page.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _visitor_visitor_service__WEBPACK_IMPORTED_MODULE_2__["VisitorService"]])
    ], SecondPage);
    return SecondPage;
}());



/***/ })

}]);
//# sourceMappingURL=second-second-module.js.map