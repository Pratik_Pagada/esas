import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app.settings';

@Injectable({
	providedIn: 'root'
})
export class VisitorGuardService {

	constructor(private _http: HttpClient) { }

	getAllUsers() {
		return this._http.get(AppSettings.BASE_URL + '/user/list');
	}

	submit(data) {
		return this._http.post<any>(AppSettings.BASE_URL + '/visitors/submitApproval', data);
	}

	getVisitors() {
		return this._http.get(AppSettings.BASE_URL + '/visitors/getAllForGuard');
	}


	visitorArrived(visitor) {
		return this._http.post<any>(AppSettings.BASE_URL + '/visitors/markArrived/' + visitor.id + "/" + visitor.cardNumber, null);
	}


	visitorLeft(visitor) {
		var data = {
			'visitorId': visitor.id,
			'cardHandedDown': visitor.cardHandedDown
		};
		return this._http.post<any>(AppSettings.BASE_URL + '/api/visitors/markLeft', data);
	}
}