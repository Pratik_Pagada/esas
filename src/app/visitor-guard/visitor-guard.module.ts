import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { VisitorGuardPage } from './visitor.page';
import { HeaderModule } from '../header/header.module';
import { LayoutComponentModule } from '../layout/layout.module';
import { VisitorGuardService } from './visitor.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		HeaderModule,
		LayoutComponentModule,
		RouterModule.forChild([
			{
				path: '',
				component: VisitorGuardPage
			}
		])
	],
	declarations: [VisitorGuardPage],
	providers: [VisitorGuardService]
})
export class VisitorGuardPageModule { }
