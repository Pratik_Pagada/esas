import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { VisitorGuardService } from './visitor.service';
import { ToastController } from '@ionic/angular';
import * as moment from 'moment'

@Component({
	selector: 'app-visitor-guard',
	templateUrl: 'visitor.page.html',
	styleUrls: ['visitor.page.scss']
})
export class VisitorGuardPage implements OnInit {

	visitors: any;
	today = moment(new Date()).format('YYYY-MM-DD');
	selected: any;
	devicesToSave = false;
	devicesForm: FormGroup;

	constructor(private _visitorGuardService: VisitorGuardService, public _toastController: ToastController) {

	}

	ngOnInit() {
		this.devicesForm = new FormGroup({
			devices: new FormArray([])
		});
		this.devices.push(new FormGroup(
			{
				visitorId: new FormControl(''),
				deviceDescription: new FormControl(''),
				deviceStays: new FormControl(false)
			}
		));
		this.refreshVisitors();

	}

	addItem() {
		this.devices.push(new FormGroup(
			{
				visitorId: new FormControl(''),
				deviceDescription: new FormControl(''),
				deviceStays: new FormControl(false)
			}
		));
	}

	get devices(): FormArray {
		return this.devicesForm.get('devices') as FormArray;
	};

	refreshVisitors(): any {
		this._visitorGuardService.getVisitors().subscribe(data => {
			this.visitors = data;
		});
	}

	select(app) {
		this.selected = app;
	}

	arrived(visitor) {
		visitor.loading = true;
		this._visitorGuardService.visitorArrived(visitor).subscribe(data => {
			visitor.arrivalDate = moment(new Date()).format('YYYY-MM-DD HH:mm');
		});
	}

	left(visitor) {
		visitor.loading = true;
		this._visitorGuardService.visitorLeft(visitor).subscribe(data => {
			visitor.leaveDate = moment(new Date()).format('YYYY-MM-DD HH:mm');
		});
	}

}
