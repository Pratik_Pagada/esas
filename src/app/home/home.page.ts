import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { PatrolService } from '../patrol/patrol.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  patrols: any;
  constructor(private _authService:AuthService, private _patrolService: PatrolService){
    //this._authService.isTokenExpired().subscribe();
    this._patrolService.getAllPatrols().subscribe(data => {
      this.patrols = data.content;
      this._patrolService.patrols = data.content;
    })
  }

  openPatrol() {
     
  }
}
