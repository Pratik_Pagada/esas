import { Component } from '@angular/core';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { FCM } from '@ionic-native/fcm/ngx';
import { Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss']
})
export class AppComponent {
	public headerLogoSrc = "assets/logo/esas.png";

	public appPages = [
		{
			title: 'Dashboard',
			url: '/dashboard',
			icon: 'home'
		},
		{
			title: 'Visitors',
			url: '/visitor',
			icon: 'person'
		},
		{
			title: 'Visitors - Guard',
			url: '/visitorguard',
			icon: 'person'
		},
		/*
		{
		  title: 'Bejárás',
		  url: '/patrol/guard',
		  icon: 'walk'
		},
		*/
		{
			title: 'Ellenőrzőpontok szerkesztése',
			url: '/patrol/admin/checks',
			icon: 'checkmark-circle-outline'
		}
	];

	constructor(
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private fcm: FCM,
		private alertController: AlertController,
		private router: Router
	) {
		this.initializeApp();
	}

	initializeApp() {
		var self = this;
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
			this.fcm.subscribeToTopic('all');
			this.fcm.getToken().then(token => {
				localStorage.setItem('deviceId', token);
				console.log("token arrived");
				console.log(token);
			});
			this.fcm.onTokenRefresh().subscribe(token => {
				localStorage.setItem('deviceId', token);
				console.log("token refreshed");
				console.log(token);
			});
			this.fcm.onNotification().subscribe(data => {
				console.log(data);
				if (data.wasTapped) {
					console.log('Received in background');
					this.router.navigate([data.landing_page, data.price]);
				} else {
					console.log('Received in foreground');
					this.router.navigate([data.landing_page, data.price]);
				}
			});
		});
	}

	async presentAlert(data) {
		const alert = await this.alertController.create({
			header: data.header,
			subHeader: data.subHeader,
			message: data.message,
			buttons: ['OK']
		});

		await alert.present();
	}
}
