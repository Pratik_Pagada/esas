import { Component, OnInit } from '@angular/core';
import { MenuController, NavController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-login',
	templateUrl: './login.page.html',
	styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

	loginCredentials: any = {};
	returnUrl: string;
	constructor(
		private _menuController: MenuController, 
		private _authService: AuthService, 
		private _navigationController: NavController, 
		private _route: ActivatedRoute
	) { }

	ngOnInit() {
		this._authService.logout();
		this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
	}

	login() {
		this._authService.login(
			this.loginCredentials.email, 
			this.loginCredentials.password
		).subscribe(data => {
			let deviceid = localStorage.getItem('deviceId');
			let value = {
				userid: data.userId,
				deviceid: deviceid
			}
			this._authService.registerDevice(value).subscribe(data => {
				this._navigationController.navigateRoot(this.returnUrl);
			});
		}, error => {
		});
	}

	ionViewWillEnter() {
		this._menuController.enable(false);
	}

	ionViewDidLeave() {
		this._menuController.enable(true);
	}

}
