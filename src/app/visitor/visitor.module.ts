import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { VisitorPage } from './visitor.page';
import { HeaderModule } from '../header/header.module';
import { LayoutComponentModule } from '../layout/layout.module';
import { VisitorService } from './visitor.service';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		IonicModule,
		HeaderModule,
		LayoutComponentModule,
		RouterModule.forChild([
			{
				path: '',
				component: VisitorPage
			}
		])
	],
	declarations: [VisitorPage],
	providers: [VisitorService]
})
export class VisitorPageModule { }
