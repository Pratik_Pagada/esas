import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { VisitorService } from './visitor.service';
import { ToastController } from '@ionic/angular';

@Component({
	selector: 'app-visitor',
	templateUrl: 'visitor.page.html',
	styleUrls: ['visitor.page.scss']
})
export class VisitorPage implements OnInit {

	visitorForm: FormGroup;
	users: any = {
		content: []
	};

	constructor(
		private _visitorService: VisitorService, 
		public _toastController: ToastController
	) { }

	ngOnInit() {
		this.refreshUsers();
		this.visitorForm = new FormGroup({
			place: new FormControl([], [Validators.required]),
			wholeEstablishment: new FormControl(false),
			access: new FormControl(''),
			host: new FormControl('', [Validators.required]),
			phoneNumber: new FormControl('', [Validators.required]),
			from: new FormControl(''),
			to: new FormControl(''),
			reason: new FormControl('', [Validators.required]),
			visitors: new FormArray([
				new FormGroup({
					name: new FormControl('', [Validators.required]),
					email: new FormControl('', null)
				})
			]),
			otherRequest: new FormControl(false),
			otherRequestDescription: new FormControl(''),
			notificationType: new FormControl('phone')
		});
	}

	submit() {
		var data = {
			"placeId": this.visitorForm.value.place[0].id,
			"hostId": this.visitorForm.value.host.id,
			"phoneNumber": this.visitorForm.value.phoneNumber,
			"from": this.visitorForm.value.from,
			"to": this.visitorForm.value.to,
			"reason": this.visitorForm.value.reason,
			"visitors": this.visitorForm.value.visitors,
			"otherRequest": this.visitorForm.value.otherRequest,
			"otherRequestDescription": this.visitorForm.value.otherRequestDescription,
			"notificationType": this.visitorForm.value.notificationType,
			"access": this.visitorForm.value.access
		};

		this._visitorService.submit(data).subscribe(data => {
			this.openSuccessSnack();
		}, error => {
			this.openErrorSnack();
		});
	}

	async openSuccessSnack() {
		const toast = await this._toastController.create({
			message: 'Művelet sikeres.',
			duration: 2000
		});
		toast.present();
	}

	async openErrorSnack() {
		const toast = await this._toastController.create({
			message: 'Művelet sikertelen, háttérrendszer hiba!',
			duration: 2000
		});
		toast.present();
	}

	refreshUsers() {
		this._visitorService.getAllUsers().subscribe(data => {
			this.users = data;
		});
	}

	get visitors(): FormArray {
		return this.visitorForm.get('visitors') as FormArray;
	};

	removeItem(index) {
		this.visitors.removeAt(index);
	}

	addItem() {
		this.visitors.push(
			new FormGroup({
				name: new FormControl('', [Validators.required]),
				email: new FormControl('', null)
			})
		);
	}

	refreshSelectedPlaces(places) {
		this.visitorForm.controls['place'].setValue(places);
	}
	// add back when alpha.4 is out
	// navigate(item) {
	//   this.router.navigate(['/list', JSON.stringify(item)]);
	// }
}
