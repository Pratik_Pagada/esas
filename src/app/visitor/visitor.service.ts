import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app.settings';

@Injectable({
    providedIn: 'root'
})
export class VisitorService {

    constructor(
        private _http: HttpClient
    ) { }

    getAllUsers() {
        return this._http.get(AppSettings.BASE_URL+'/user/list');
    }

    submit(data){
        return this._http.post<any>(AppSettings.BASE_URL+'/visitors/submitApproval', data);
    }

    answer(data){
        return this._http.post<any>(AppSettings.BASE_URL+'/visitors/notificationResponse', data);
    }
}