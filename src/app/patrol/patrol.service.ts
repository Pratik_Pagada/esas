import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../app.settings';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PatrolService {

    actions = [];
    checks = [];
    public patrols = [];
    public checkSubscription: Subject<any> = new Subject();

    constructor(private _http: HttpClient) { }


    getAllUsers() {
        return this._http.get(AppSettings.BASE_URL+'/user/list');
    }

    submit(data){
        return this._http.post<any>(AppSettings.BASE_URL+'/visitors/submitApproval', data);
    }

    getChecks() {
        if(this.checks.length == 0){
            this.refreshChecks();
        }
        else {
            this.checkSubscription.next(this.checks);
        }
    }

    saveAttribute(attribute) {
        this._http.post(AppSettings.PATROL_BASE_URL+'/patrol/createCheckAttribute',attribute, {responseType: 'text'}).subscribe(data =>{
            if(attribute.files && attribute.files.length >0) {
                this.uploadFiles(data,'PATROL_CHECK_ATTRIBUTE_PHOTO',attribute.files);
            } else {
                this.refreshChecks();
            }
        });
    }

    refreshChecks() {
        this._http.get<any>(AppSettings.PATROL_BASE_URL+'/patrol/getAllChecks').subscribe(data => {
            this.checks = data.content;
            this.checkSubscription.next(this.checks);
        });
    }

    addCheck(check) {
        return this._http.post<any>(AppSettings.PATROL_BASE_URL+'/patrol/createCheck', check);
    }

    getAllActions() {
        return this._http.get<any>(AppSettings.PATROL_BASE_URL+'/patrol/getAllActions');
    }

    removeCheck(check) {
        var index = this.checks.indexOf(check);
        if (index > -1) {
            this.checks.splice(index, 1);
        }
    }

    getAllPatrols() {
        return this._http.get<any>(AppSettings.PATROL_BASE_URL+'/patrol/getAllPatrols');
    }

     getPatrolById(id: any) {
        return this._http.get<any>(AppSettings.PATROL_BASE_URL+'/patrol/getPatrolById/'+id);
        
    }

    startPatrolInstance(instanceId) {
        return this._http.post<any>(AppSettings.PATROL_BASE_URL+'/patrol/startPatrolInstance/'+instanceId,null);
    }

    finishPatrolInstance(instanceId: any): any {
        return this._http.post<any>(AppSettings.PATROL_BASE_URL+'/patrol/finishPatrolInstance/'+instanceId,null);
    }

    getPatrolInstanceById(instanceId){
        return this._http.get<any>(AppSettings.PATROL_BASE_URL+'/patrol/getPatrolInstanceById/'+instanceId);
    }

    startCheckInstance(id: any): any {
        return this._http.post<any>(AppSettings.PATROL_BASE_URL+'/patrol/startCheckInstance/'+id,null);
    }

    saveCheckAttributeInstance(data) {
        return this._http.post<any>(AppSettings.PATROL_BASE_URL+'/patrol/saveCheckAttributeInstance',data);
    }

    saveIrregularity(data) {
        return this._http.post(AppSettings.PATROL_BASE_URL+'/patrol/addIrregularity',data, {responseType: 'text'});
    }

    skipCheckInstance(data) {
        return this._http.post<any>(AppSettings.PATROL_BASE_URL+'/patrol/skipCheckInstance',data);
    }

    uploadFile(id,component,file) {
        const formData: FormData = new FormData();
        formData.append('encriptedId',id);
        formData.append('component',component);
        formData.append('file', file, file.name);
        return this._http.post(AppSettings.PATROL_BASE_URL+'/patrol/uploadFiles', formData)
      }
    
      uploadFiles(id,component, files){
        const formData: FormData = new FormData();
        formData.append('encriptedId',id);
        formData.append('component',component);
        for (let file of files) {
          formData.append('file', file, 'dummy');
        }
        return this._http.post(AppSettings.PATROL_BASE_URL+'/patrol/uploadFiles', formData)
      }
}