import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';

import { ToastController } from '@ionic/angular';
import { PatrolService } from '../patrol.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-patrol-guard',
  templateUrl: 'patrol-guard.page.html',
  styleUrls: ['patrol-guard.page.scss']
})
export class PatrolGuardPage implements OnInit {

  skip = false;
  inProgress = false;
  finished = false;
  progress = 1;
  customPerception = false;
  patrolId = null;
  instanceId = null;
  loading=false;
  currentPatrol: any;
  private sub: any;

  checkPoints = [];

  constructor(private _patrolService: PatrolService, public _toastController: ToastController, private route: ActivatedRoute) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.patrolId = params['id'];
      this.instanceId = params['instanceId'];
      this._patrolService.getPatrolById(params['id']).subscribe(data => {
        this.currentPatrol = data;
      }); // (+) converts string 'id' to a number
      
   });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  startPatrol(){
    this.loading = true;
    if(this.currentPatrol.instances[0].status == 'PENDING'){
      this._patrolService.startPatrolInstance(this.instanceId).subscribe(data => {
        this._patrolService.getPatrolInstanceById(this.instanceId).subscribe(instance => {
          this.inProgress = true;
          this.checkPoints = instance.specificCheckInstances;
          this.loading = false;
        });
      });
    }
    else {
      this._patrolService.getPatrolInstanceById(this.instanceId).subscribe(instance => {
        this.inProgress = true;
        this.checkPoints = instance.specificCheckInstances;
        this.loading = false;
      });
    }
  }

  
  onFinished() {
    if(this.checkPoints[this.progress]){
      this.progress++;
    } else {
      this.loading = true;
      this._patrolService.finishPatrolInstance(this.instanceId).subscribe(data => {
        this.finished = true;
        this.loading = false;
      });
    }
  }

  onCheckpointStarted(event) {
    this.loading = true;
    this._patrolService.getPatrolInstanceById(this.instanceId).subscribe(instance => {
      this.checkPoints = instance.specificCheckInstances;
      this.loading = false;
    });
  }

  get currentCheckpoint() {
    return this.checkPoints[this.progress-1];
  }
  


}
