import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { DomSanitizer } from "@angular/platform-browser";
import { PatrolService } from "../../patrol.service";
import { AppSettings } from "src/app/app.settings";

@Component({
    selector: 'app-patrol-guard-checkpoint',
    templateUrl: 'patrol-checkpoint.component.html',
    styleUrls: ['patrol-checkpoint.component.scss']
  })
  export class PatrolCheckpointComponent implements OnInit {
  
    @Input() checkpoint: any;
    @Input() progress: any;
    @Output() finishedCheckpoint = new EventEmitter<boolean>();
    @Output() startedCheckpoint = new EventEmitter<boolean>();
  
    images = [];
    choice = null;
    description = '';
    started = false;
    loading = false;
    criteriaProgress = 1;
    constructor( private camera: Camera, private domSanitizer: DomSanitizer, private _patrolService : PatrolService) {
  
    }
  
    ngOnInit() {
        console.log(this.checkpoint);
    }

    startCheckpoint() {
        this.loading = true;
        this._patrolService.startCheckInstance(this.checkpoint.id).subscribe(data => {    
            this.startedCheckpoint.emit(true);
            this.started = true;
            this.criteriaProgress = 1;
            this.loading = false;
        });
    }

    ok() {
        this.choice = 1;
    }

    nok() {
        this.choice = -1;
    }

    skip() {
        this.choice = 0;
    }

    sendResultValid() {
        var valid = true;
        this.actions.forEach(element => {
            if(!element.checked){
                valid = false;
            }
        });
        return this.choice != -1 || true || (valid && (this.images.length > 0));
    }

    sendChoice() {
        var data = null;
        if(this.choice == 0){
            /*
            var data = {
                instanceId: this.checkpoint.id,
                skipReason: this.description
            }
            this._patrolService.skipCheckInstance();
            */
        }
        this.loading = true;
        if(this.choice == -1){
            data = {
                checkAttributeInstancekId: this.currentCriteria.id,
                description: this.description,
                photos: this.images,
                severity: "LOW",
                responsibleUserId: "jJKXJyKzwm"
            }
            this._patrolService.saveIrregularity(data).subscribe(d => {
                data = {
                    checkAttributeInstanceId: this.currentCriteria.id,
                    result: "IRREGULAR",
                    photos: this.images
                };
                this._patrolService.saveCheckAttributeInstance(data).subscribe(z => {
                    
                    console.log("step");
                    console.log(this.checkpoint.checkAttributeInstances.length);
                    if(this.checkpoint.checkAttributeInstances[this.criteriaProgress]) {
                        this.criteriaProgress++;
                    } else {
                        this.started = false;
                        this.finishedCheckpoint.emit(true);
                    }
                    this.description = '';
                    this.images = [];
                    this.choice = null;
                    this.loading = false;
                });
            });
        }
        else if(this.choice == 1){
            data = {
                checkAttributeInstanceId: this.currentCriteria.id,
                result: "OK",
                photos: this.images
            };
            this._patrolService.saveCheckAttributeInstance(data).subscribe(d => {
                if(this.checkpoint.checkAttributeInstances[this.criteriaProgress]) {
                    this.criteriaProgress++;
                } else {
                    this.started = false;
                    this.finishedCheckpoint.emit(true);
                }
                this.description = '';
                this.images = [];
                this.choice = null;
                this.loading = false;
            });
        }
        
    }

    get currentCriteria() {
        return this.checkpoint.checkAttributeInstances[this.criteriaProgress-1];
    }

    takePicture() {
        var options: CameraOptions = {
            quality: 30,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
     
        this.camera.getPicture(options).then(imageData => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.images.push(base64Image);
        });
     
    }

    removeImage(index) {
        this.images.splice(index, 1);
    }

    get actions() {
        return this.currentCriteria.checkAttribute.actions;
    }

    get apiUrl() {
        return AppSettings.PATROL_BASE_URL+'/';
    }
  
  }