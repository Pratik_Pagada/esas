import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { HeaderModule } from '../../header/header.module';
import { LayoutComponentModule } from '../../layout/layout.module';
import { PatrolService } from '../patrol.service';
import { PatrolGuardPage } from './patrol-guard.page';
import { PatrolCheckpointComponent } from './checkpoint/patrol-checkpoint.component';
import { CustomPerceptionComponent } from './custom-perception/custom-perception.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    HeaderModule,
    LayoutComponentModule,
    RouterModule.forChild([
      {
        path: '',
        component: PatrolGuardPage
      }
    ])
  ],
  declarations: [PatrolGuardPage,PatrolCheckpointComponent, CustomPerceptionComponent],
  providers: [PatrolService]
})
export class PatrolGuardPageModule {}
