import { Component, OnInit } from "@angular/core";
import { ActionSheetController } from "@ionic/angular";
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { DomSanitizer } from "@angular/platform-browser";

@Component({
    selector: 'app-patrol-custom-perception',
    templateUrl: 'custom-perception.component.html',
    styleUrls: ['custom-perception.component.scss']
  })
  export class CustomPerceptionComponent implements OnInit {

    description = '';
    images = [];
    constructor(private camera: Camera, private domSanitizer: DomSanitizer) {
  
    }
  
    ngOnInit() {
    }

    takePicture() {
        var options: CameraOptions = {
            quality: 30,
            sourceType: this.camera.PictureSourceType.CAMERA,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
     
        this.camera.getPicture(options).then(imageData => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.images.push(base64Image);
        });
     
    }

    removeImage(index) {
        this.images.splice(index, 1);
    }
  
  }