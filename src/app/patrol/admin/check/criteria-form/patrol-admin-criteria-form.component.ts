import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { ToastController } from '@ionic/angular';
import { PatrolService } from 'src/app/patrol/patrol.service';
import { DomSanitizer } from '@angular/platform-browser';
import { File } from '@ionic-native/File/ngx';
import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet/ngx';

declare var cordova: any;

@Component({
  selector: 'app-patrol-admin-criteria-form',
  templateUrl: 'patrol-admin-criteria-form.component.html',
  styleUrls: ['patrol-admin-criteria-form.component.scss']
})
export class PatrolAdminCriteriaFormComponent implements OnInit {

    @Input() criteria: any;
    @Output() finishedEdit = new EventEmitter<boolean>();
    
    actions: any;
    files = [];
    ngOnInit(): void {

      this._patrolService.getAllActions().subscribe(data => {
        this.actions = data.content;
      });
      if(this.criteria.id){
        if(!this.criteria.actionIdList){
          this.criteria.actionIdList = [];
        }
        this.criteria.actions.forEach(data => {
          this.criteria.actionIdList.push(data.id);
        });
      }
    }

    constructor(private _patrolService : PatrolService, private camera: Camera, private domSanitizer: DomSanitizer, private file: File, private toastCtrl: ToastController,  private actionSheet : ActionSheet){

    }

    takePicture() {
      let buttonLabels = ['Fénykép készítése', 'Galéria'];

      const options: ActionSheetOptions = {
          title: 'Kép forrása',
          buttonLabels: buttonLabels,
          addCancelButtonWithLabel: 'Cancel',
          destructiveButtonLast: true
      }

      this.actionSheet.show(options).then((buttonIndex: number) => {
          if(buttonIndex == 1){
              this.doTakePicture(this.camera.PictureSourceType.CAMERA);
          } else {
              this.doTakePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
      });
  }

  doTakePicture(sourceType) {
      var options: CameraOptions = {
          quality: 30,
          sourceType: sourceType,
          destinationType: this.camera.DestinationType.DATA_URL,
          mediaType: this.camera.MediaType.PICTURE,
          saveToPhotoAlbum: false,
          correctOrientation: true,
          targetWidth: 800
      };
   
      this.camera.getPicture(options).then(imageData => {
        let base64Image = 'data:image/jpeg;base64,' + imageData;
        if(!this.criteria.photos){
          this.criteria.photos = [];
        }
        if(!this.criteria.files) {
          this.criteria.files = [];
        }
        this.criteria.photos.push(base64Image);
        this.criteria.files.push(base64Image);
    });
  }

  dataURLtoBlob(dataurl) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new Blob([u8arr], {type:mime});
}

  addAction(){
    if(!this.criteria.actionIdList){
      this.criteria.actionIdList = [];
    }
    this.criteria.actionIdList.push(this.actions[0].id);
  }

    removeImage(index) {
        this.criteria.photos.splice(index, 1);
    }

    save() {
        this.finishedEdit.emit(this.criteria);
    }

    private copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.criteria.photos.push(newFileName);
        }, error => {

        });
    }

    private createFileName() {
        var d = new Date(),
        n = d.getTime(),
        newFileName =  n + ".jpg";
        return newFileName;
      }

       
      // Always get the accurate path to your apps folder
      public pathForImage(img) {
        if (img === null) {
          return '';
        } else {
          return cordova.file.dataDirectory + img;
        }
      }


}