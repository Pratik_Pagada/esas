import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/Camera/ngx';
import { ToastController, AlertController } from '@ionic/angular';
import { PatrolService } from 'src/app/patrol/patrol.service';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet/ngx';


@Component({
  selector: 'app-patrol-admin-check-form',
  templateUrl: 'patrol-admin-check-form.component.html',
  styleUrls: ['patrol-admin-check-form.component.scss']
})
export class PatrolAdminCheckFormComponent implements OnInit {

    @Input() checkpoint: any;
    @Input() new: any;
    @Output() finishedEdit = new EventEmitter<boolean>();
    selectedCriteria = null;
    newCrit = false;
    editCrit = false;
    default = null;
    file:any;
    ngOnInit(): void {
        this.default = this.checkpoint.place.id;
    }

    constructor(private _patrolService : PatrolService, private camera: Camera, private domSanitizer: DomSanitizer, private actionSheet : ActionSheet, private alertCtrl: AlertController){
        
    }

    refreshPlace(place) {
        this.checkpoint.placeId = place;
    }

    takePicture() {
        let buttonLabels = ['Fénykép készítése', 'Galéria'];

        const options: ActionSheetOptions = {
            title: 'Kép forrása',
            buttonLabels: buttonLabels,
            addCancelButtonWithLabel: 'Cancel',
            destructiveButtonLast: true
        }

        this.actionSheet.show(options).then((buttonIndex: number) => {
            if(buttonIndex == 1){
                this.doTakePicture(this.camera.PictureSourceType.CAMERA);
            } else {
                this.doTakePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            }
        });
    }

    doTakePicture(sourceType) {
        var options: CameraOptions = {
            quality: 30,
            sourceType: sourceType,
            destinationType: this.camera.DestinationType.DATA_URL,
            mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            targetWidth: 800
        };
     
        this.camera.getPicture(options).then(imageData => {
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            this.checkpoint.photo = base64Image;
            this.file = this.dataURLtoBlob(base64Image);
        });
    }

    dataURLtoBlob(dataurl) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new Blob([u8arr], {type:mime});
    }

    editCriteria(criteria) {
        this.selectedCriteria = criteria;
    }

    deleteCriteria(criteria) {
        var index = this.checkpoint.criteria.indexOf(criteria);
        if (index > -1) {
            this.checkpoint.criteria.splice(index, 1);
        }
    }

    newCriteria() {
        this.selectedCriteria = {
            name: '',
            description: '',
            photos: [],
            actions: [],
            actionIdList: [],
            photoMandatory: false
        }
        this.newCrit = true;
        this.editCrit = true;
    }

    onFinished(event) {
        if(this.newCrit){
            this.selectedCriteria.checkId = this.checkpoint.id;
            this.checkpoint.checkAttributes.push(this.selectedCriteria);
            this._patrolService.saveAttribute(this.selectedCriteria);
            this._patrolService.refreshChecks();
        }
        this.newCrit = false;
        this.selectedCriteria = null;
    }

    save() {
        if(this.new) {
            this._patrolService.addCheck(this.checkpoint).subscribe(data => {
                if(this.file) {
                    this._patrolService.uploadFile(data.id,'PATROL_CHECK_PHOTO',this.file).subscribe(data =>{

                    });
                }
                this._patrolService.refreshChecks();
            });
        }
        this.finishedEdit.emit(true);
    }

    



}