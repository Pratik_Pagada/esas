import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormGroup, FormControl, Validators } from '@angular/forms';

import { ToastController } from '@ionic/angular';
import { PatrolService } from '../../patrol.service';


@Component({
  selector: 'app-patrol-admin-check',
  templateUrl: 'patrol-admin-check.page.html',
  styleUrls: ['patrol-admin-check.page.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class PatrolAdminCheckPage implements OnInit {


    checks = [];
    selectedCheckpoint = null;
    edit = false;
    new = false;

    ngOnInit(): void {
        this._patrolService.checkSubscription.subscribe(data => {
            this.checks = []
            this.checks = data;
            console.log("updated");
            console.log(this.checks);
        });
        this.refreshChecks();
    }

    constructor(private _patrolService : PatrolService){

    }

    private refreshChecks() {
        this._patrolService.getChecks();
    }

    deleteCheck(check) {
        this._patrolService.removeCheck(check);
        this.refreshChecks();
    }

    createCheck() {
        this.selectedCheckpoint = {
            name: '',
            place: '',
            image: null,
            criteria: []
        };
        this.new = true;
    }

    editCheck(check) {
        this.selectedCheckpoint = check;
        this.edit = true;
    }

    onFinished(event) {
        this.new = false;
        this.edit = false;
        this.selectedCheckpoint = null;
        this.refreshChecks();
      }

}