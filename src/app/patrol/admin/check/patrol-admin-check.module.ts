import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { LayoutComponentModule } from "src/app/layout/layout.module";
import { HeaderModule } from "src/app/header/header.module";
import { PatrolAdminCheckPage } from "./patrol-admin-check.page";
import { PatrolService } from "../../patrol.service";
import { PatrolAdminCheckFormComponent } from "./check-form/patrol-admin-check-form.component";
import { PatrolAdminCriteriaFormComponent } from "./criteria-form/patrol-admin-criteria-form.component";


@NgModule({
    imports: [
      CommonModule,
      FormsModule,
      ReactiveFormsModule,
      IonicModule,
      HeaderModule,
      LayoutComponentModule,
      RouterModule.forChild([
        {
          path: '',
          component: PatrolAdminCheckPage
        }
      ])
    ],
    declarations: [PatrolAdminCheckPage, PatrolAdminCheckFormComponent,PatrolAdminCriteriaFormComponent],
    providers: [PatrolService]
  })
  export class PatrolAdminCheckPageModule {

  }