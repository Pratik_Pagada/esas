export class AppSettings {
    private static HOST_LOCAL = 'localhost';
    private static HOST_PC = '192.168.1.70';
    private static HOST_AWS = '18.197.166.53';
    private static HOST_SCALEWAY = '51.15.192.122';
    private static HOST = AppSettings.HOST_SCALEWAY;
    //public static BASE_URL='http://'+AppSettings.HOST+':8889/api';
    public static BASE_URL='http://'+AppSettings.HOST+':8889/api';
    public static PATROL_BASE_URL='http://'+AppSettings.HOST+':8889/patrol-api';
    //public static NOTIFICATION_BASE_URL='http://'+AppSettings.HOST+':8889/notification-api';
    public static NOTIFICATION_BASE_URL='http://'+AppSettings.HOST+':8889/notification-api';
}