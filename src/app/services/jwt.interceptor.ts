import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private storage: Storage) { };

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let promise = this.storage.get('currentUser');
        return from(promise).pipe(
            mergeMap(value => {
                let currentUser = JSON.parse(value);
                if (currentUser && currentUser.token) {
                    request = request.clone({
                        setHeaders: {
                            Authorization: `Bearer ${currentUser.token}`
                        }
                    });
                }

                return next.handle(request).pipe(
                    map(event => {
                        console.log(event);
                        console.log((<HttpResponse<any>>event).headers);
                        if (event instanceof HttpResponse && event.headers.get("Authorization")) {
                            currentUser.token = event.headers.get("Authorization");
                            console.log("updating token");
                            console.log(currentUser);
                            this.storage.set('currentUser', JSON.stringify(currentUser));
                        }
                        return event;
                    }),
                    catchError(err => throwError(err))
                );
            }), catchError(err => throwError(err))
        );
    }
}