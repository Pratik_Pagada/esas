import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { Router } from '@angular/router';
import { AppSettings } from '../app.settings';
import { Storage } from '@ionic/storage';
 
@Injectable()
export class UnauthorizedInterceptor implements HttpInterceptor {

    constructor(private router: Router, private storage: Storage) { }
    
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.storage.get('currentUser').then(value=>{
                        if (value) {
                            let currentUser = JSON.parse(value);
                            currentUser.token = event.headers.get('Authorization');
                            this.storage.set('currentUser', JSON.stringify(currentUser));
                        }
                    });
                }
            }, (err: any) => {
                if (err instanceof HttpErrorResponse && request.url !== AppSettings.BASE_URL+'/security/logIn') {
                    if (err.status === 401) {
                        this.router.navigate(['/login']);
                    }
                }
            })
        );
    }
}