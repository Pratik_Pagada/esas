import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AppSettings } from '../app.settings';
import { throwError } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private http: HttpClient, private storage: Storage) { }

	login(email: string, password: string) {
		return this.http.post<any>(AppSettings.BASE_URL + '/security/logIn', { email: email, password: password }).pipe(
			map(user => {
				// login successful if there's a jwt token in the response
				if (user && user.token) {
					// store user details and jwt token in local storage to keep user logged in between page refreshes
					this.storage.set('currentUser', JSON.stringify(user));
				}

				return user;
			}),
			catchError(err => throwError(err)),
		);
	}

	logout() {
		// remove user from local storage to log user out
		this.storage.remove('currentUser');
	}

	changePassword(password: string) {
		return this.storage.get('currentUser').then((value) => {
			let currentUser = JSON.parse(value);
			return this.http.post<any>(AppSettings.BASE_URL + '/user/updatePassword', { id: currentUser.userId, password: password });
		});
	}

	currentUserHasGroup(group) {
		return this.storage.get('currentUser').then((value) => {
			if (value) {
				let currentUser = JSON.parse(value);
				return currentUser.groups.includes(group);
			}
			return false;
		});
	}

	currentUserHasMenu(menuAlias) {
		return this.storage.get('currentUser').then((value) => {
			if (value) {
				let currentUser = JSON.parse(value);
				return currentUser.menuElements.includes(menuAlias);
			}
			return false;
		});
	}

	isTokenExpired() {
		return this.http.get(AppSettings.BASE_URL + '/security/tokenExpired');
	}

	registerDevice(data) {
		return this.http.post(AppSettings.NOTIFICATION_BASE_URL + '/users/create', data, { responseType: 'text' });
	}
}