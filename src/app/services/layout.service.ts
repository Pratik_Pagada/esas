import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AppSettings } from '../app.settings';
import { of } from 'rxjs/internal/observable/of';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  places: any = [];
  public placeSubscription: Subject<any> = new Subject();
  constructor(private http: HttpClient) { }

  refreshPlaces() {
    if(this.places.length == 0){
      this.http.get(AppSettings.BASE_URL+'/place/places').subscribe(data =>{
        this.places = data;
        this.placeSubscription.next(this.places);
      });
    }
    else {
      this.placeSubscription.next(this.places);
    }
  }
}