import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router, private storage: Storage) { }
 
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.storage.get('currentUser').then((value) => {
            if (value) {
                let currentUser = JSON.parse(value);
                /*
                if(route.routeConfig.path.search('guard') == -1 && currentUser.groups[0] == 'security_guard'){
                this.router.navigate(['/guard']);
                }
                */
                /*
                else if(route.routeConfig.path == 'guard') {
                    this.router.navigate(['/dashboard']);
                }
                */
                return true;
            }
    
            // not logged in so redirect to login page with the return url
            this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
            return false;
        });
    }
}