import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AuthService } from './services/auth.service';
import { LayoutService } from './services/layout.service';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';

import { JwtInterceptor } from './services/jwt.interceptor';
import { UnauthorizedInterceptor } from './services/unauthorized.interceptor';

import { AuthGuard } from './services/auth.guard'
import { HeaderModule } from './header/header.module';

import { IonicStorageModule } from '@ionic/storage';

import { Camera } from '@ionic-native/Camera/ngx';
import { File } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { ActionSheet } from '@ionic-native/action-sheet/ngx';

import { FCM } from '@ionic-native/fcm/ngx';

@NgModule({
	declarations: [AppComponent],
	entryComponents: [],
	imports: [
		HeaderModule,
		BrowserModule,
		IonicModule.forRoot(),
		AppRoutingModule,
		HttpClientModule,
		IonicStorageModule.forRoot()
	],
	providers: [
		LayoutService,
		AuthService,
		StatusBar,
		SplashScreen,
		HttpClient,
		AuthGuard,
		Camera,
		File,
		WebView,
		FilePath,
		ActionSheet,
		FCM,
		{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
		{
			provide: HTTP_INTERCEPTORS,
			useClass: UnauthorizedInterceptor,
			multi: true
		},
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtInterceptor,
			multi: true
		},
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
