import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';
import { LayoutService } from '../services/layout.service';

@Component({
  selector: 'app-layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['layout.component.scss']
})
export class LayoutComponent implements OnInit {

  @Output() placeSelected = new EventEmitter<String[]>();

  @Input() multi;
  @Input() default: any;

  places: any = [];
  selectedPlaces: any;
  
  constructor(private _layoutService: LayoutService) {

  }

  ngOnInit() {
    this._layoutService.placeSubscription.subscribe(data => {
      this.places = data;
    });
    this.refreshPlaces();
    if(this.multi) {
      this.selectedPlaces = [];
      if(this.default) {
        for (var i = 0, len = this.places.length; i < len; i++) {
          if(this.places[i].id === this.default.id){
            this.selectedPlaces.push(this.places[i].id);
          }
        }
        
      }
    }
    else {
      this.selectedPlaces = '';
      if(this.default) {
        for (var i = 0, len = this.places.length; i < len; i++) {
          if(this.places[i].id == this.default){
            this.selectedPlaces = this.places[i].id;
          }
        }
      }
    }
  }

  refreshPlaces() {
    this._layoutService.refreshPlaces();
  }

  placeChange(newValue) {
    this.selectedPlaces = newValue;
    this.placeSelected.emit(this.selectedPlaces);
  }

  

  isSelectedPlace(placeName) {
    if(this.multi) {
    let arr = this.selectedPlaces;
    for(var i = 0; i < arr.length; i++) {
      if(arr[i].englishName === placeName){
        return true;
      }
    } 
  }
  else {
    for (var i = 0, len = this.places.length; i < len; i++) {
      if(this.places[i].englishName === placeName){
        return this.selectedPlaces === this.places[i].id;
      }
    }
  }
    //return this.newKeyForm.value.place.englishName === placeName;
  }

  setPlace(placeName) {
    if(this.multi) {
      for (var i = 0, len = this.places.length; i < len; i++) {
        if(this.places[i].englishName === placeName){
          let arr = this.selectedPlaces;
          let index = arr.indexOf(this.places[i].id);
          if(index > -1){
            arr.splice(index,1);
          } else {
            arr.push(this.places[i].id);
          }
          this.selectedPlaces = [];
          this.selectedPlaces = arr;
          break;
        }
      }
      this.placeSelected.emit(this.selectedPlaces);
    } else {
      for (var i = 0, len = this.places.length; i < len; i++) {
        if(this.selectedPlaces === this.places[i].id) {
          this.selectedPlaces = '';
          this.placeSelected.emit(this.selectedPlaces);
          return;
        } else if(this.places[i].englishName === placeName){
          this.selectedPlaces = this.places[i].id;
          this.placeSelected.emit(this.selectedPlaces);
          return;
        } 
      }
      
    }
  }

}