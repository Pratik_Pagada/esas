import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
	{
		path: '',
		redirectTo: 'dashboard',
		pathMatch: 'full'
	},
	{
		path: 'dashboard',
		loadChildren: './home/home.module#HomePageModule',
		canActivate: [AuthGuard],
	},
	{
		path: 'visitor',
		loadChildren: './visitor/visitor.module#VisitorPageModule',
		canActivate: [AuthGuard],
	},
	{
		path: 'visitorguard',
		loadChildren: './visitor-guard/visitor-guard.module#VisitorGuardPageModule',
		canActivate: [AuthGuard],
	},
	{
		path: 'patrol/guard/:id/:instanceId',
		loadChildren: './patrol/guard/patrol-guard.module#PatrolGuardPageModule',
		canActivate: [AuthGuard],
	},
	{
		path: 'patrol/admin/checks',
		loadChildren: './patrol/admin/check/patrol-admin-check.module#PatrolAdminCheckPageModule',
		canActivate: [AuthGuard],
	},
	{ path: 'login', loadChildren: './login/login.module#LoginPageModule' },
	{ path: 'second/:price', loadChildren: './second/second.module#SecondPageModule' },
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
